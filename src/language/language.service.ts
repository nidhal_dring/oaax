import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateLanguageDto } from './dto/create-language.dto';
import { UpdateLanguageDto } from './dto/update-language.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { LanguageRepository } from './repository/language.repository';
import { Language } from './entities/language.entity';
import { Errors } from 'src/commun/enums/Errors.enum';

@Injectable()
export class LanguageService {
  constructor(
    @InjectRepository(LanguageRepository)
    private readonly languageRepository: LanguageRepository,
  ) {}

  createLanguage(createLanguageDto: CreateLanguageDto): Promise<Language> {
    return this.languageRepository.createLanguage(createLanguageDto);
  }

  findAll(): Promise<Language[]> {
    return this.languageRepository.findAllLanguages();
  }

  findOne(id: string): Promise<Language> {
    return this.languageRepository.findLanguage(id);
  }

  update(id: string, updateLanguageDto: UpdateLanguageDto): Promise<Language> {
    return this.languageRepository.updateLanguage(id, updateLanguageDto);
  }

  remove(id: string): Promise<void> {
    return this.languageRepository.deleteLanguage(id);
  }

  async extractLanguages(languages: string[]): Promise<Language[]> {
    const languagesArray = [];
    for (let i = 0; i < languages.length; i++) {
      const language = await this.findOne(languages[i]);

      if (!language) {
        throw new NotFoundException(Errors.LANGUAGE_NOT_FOUND);
      }
      languagesArray.push(language);
    }
    return languagesArray;
  }

  async deleteLeadLanguages(id: string): Promise<void> {
    await this.languageRepository.deleteLeadLanguages(id);
  }
}
