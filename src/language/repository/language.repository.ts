import { DeleteResult, EntityRepository, Repository } from 'typeorm';
import { NotFoundException } from '@nestjs/common';
import { Errors } from '../../commun/enums/Errors.enum';
import { Language } from '../entities/language.entity';
import { CreateLanguageDto } from '../dto/create-language.dto';
import { UpdateLanguageDto } from '../dto/update-language.dto';

@EntityRepository(Language)
export class LanguageRepository extends Repository<Language> {
  async createLanguage(
    createLanguageDto: CreateLanguageDto,
  ): Promise<Language> {
    const language = new Language();
    Object.assign(language, createLanguageDto);
    return await this.save(language);
  }

  async findAllLanguages(): Promise<Language[]> {
    return this.find();
  }

  async findLanguage(id: string): Promise<Language> {
    const language = await this.findOne(id);
    if (!language) {
      throw new NotFoundException(Errors.LANGUAGE_NOT_FOUND);
    }
    return language;
  }

  async updateLanguage(
    id: string,
    updateLanguageDto: UpdateLanguageDto,
  ): Promise<Language> {
    const language = await this.findLanguage(id);
    Object.assign(language, updateLanguageDto);
    return this.save(language);
  }

  async deleteLanguage(id: string): Promise<void> {
    const result = await this.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(Errors.LANGUAGE_NOT_FOUND);
    }
  }

  async deleteLeadLanguages(leadId: string): Promise<void> {
    await this.createQueryBuilder()
      .delete()
      .from('lead_languages_language')
      .where('leadId = :leadId', { leadId })
      .execute();
  }
}
