import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { LanguageService } from './language.service';
import { CreateLanguageDto } from './dto/create-language.dto';
import { UpdateLanguageDto } from './dto/update-language.dto';
import { Language } from './entities/language.entity';
import { ApiBearerAuth, ApiHeader, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { PermissionGuard } from '../permission/guard/permission.guard';
import { PermissionRequired } from '../permission/decorator/permission.decorator';
import { PermissionEnum } from '../permission/enum/permission.enum';
import { Languages } from 'src/commun/enums/languages.enum';

@Controller('api/language')
@ApiTags('Language')
@ApiHeader({ name: 'Accept-Language', enum: Languages })
export class LanguageController {
  constructor(private readonly languageService: LanguageService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  // @PermissionRequired(
  //   PermissionEnum.CREATE_LANGUAGES,
  //   PermissionEnum.MANAGE_LANGUAGES,
  // )
  create(@Body() createLanguageDto: CreateLanguageDto): Promise<Language> {
    return this.languageService.createLanguage(createLanguageDto);
  }

  @Get()
  findAll(): Promise<Language[]> {
    return this.languageService.findAll();
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  // @PermissionRequired(
  //   PermissionEnum.VIEW_LANGUAGES,
  //   PermissionEnum.MANAGE_LANGUAGES,
  // )
  findOne(@Param('id') id: string): Promise<Language> {
    return this.languageService.findOne(id);
  }

  @Patch(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  // @PermissionRequired(
  //   PermissionEnum.UPDATE_LANGUAGES,
  //   PermissionEnum.MANAGE_LANGUAGES,
  // )
  update(
    @Param('id') id: string,
    @Body() updateLanguageDto: UpdateLanguageDto,
  ): Promise<Language> {
    return this.languageService.update(id, updateLanguageDto);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  // @PermissionRequired(
  //   PermissionEnum.DELETE_LANGUAGES,
  //   PermissionEnum.MANAGE_LANGUAGES,
  // )
  remove(@Param('id') id: string): Promise<void> {
    return this.languageService.remove(id);
  }
}
