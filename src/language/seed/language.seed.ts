import { Connection, Repository } from 'typeorm';
import { Language } from '../entities/language.entity';
import langs from './languages.json';

export class LanguageSeed {
  private readonly connection: Connection;
  private readonly langRepository: Repository<Language>;

  constructor(connection: Connection) {
    this.connection = connection;
    this.langRepository = connection.getRepository(Language);
  }

  async run(): Promise<void> {
    await this.deleteAllLangs();
    await this.createLangs();
  }

  async deleteAllLangs(): Promise<void> {
    await this.langRepository.delete({});
    console.log('All langs delete');
  }

  async createLangs(): Promise<void> {
    await Promise.all(
      langs.map((lang) => {
        return this.langRepository.save(lang as Language);
      }),
    );
  }
}
