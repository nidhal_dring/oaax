export enum UserStatus {
  PENDING = 'PENDING',
  INCOMPLETE = 'INCOMPLETE',
  COMPLETE = 'COMPLETE',
}
