import {
  IsBoolean,
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateUserDto {
  @IsEmail()
  @IsOptional()
  email: string;

  @IsOptional()
  @IsEmail()
  secondaryEmail: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  lastName: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  pseudonym: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  password: string;

  @IsOptional()
  @ApiProperty({ default: new Date() })
  @IsDateString()
  dateOfBirth: Date;

  @IsOptional()
  @IsBoolean()
  status: boolean;

  @IsOptional()
  @ApiProperty({ default: 'a97bab29-1971-4a7f-83a8-06b20a4bd733' })
  @IsUUID()
  company: string;

  @IsOptional()
  @ApiProperty({ default: 'a97bab29-1971-4a7f-83a8-06b20a4bd733' })
  @IsUUID()
  role: string;
}
