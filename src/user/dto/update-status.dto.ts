import { IsEnum } from 'class-validator';
import { UserStatus } from '../enum/user-status.enum';

export class UpdateStatusDto {
  @IsEnum(UserStatus)
  status: UserStatus;
}
