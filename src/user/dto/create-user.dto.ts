import {
  IsBoolean,
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsString,
  IsUUID,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @IsEmail()
  email: string;

  @IsEmail()
  secondaryEmail: string;

  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsString()
  @IsNotEmpty()
  lastName: string;

  @IsString()
  @IsNotEmpty()
  pseudonym: string;

  @IsString()
  @IsNotEmpty()
  password: string;

  @ApiProperty({ default: new Date() })
  @IsDateString()
  dateOfBirth: Date;

  @IsBoolean()
  isActive: boolean;

  @ApiProperty({ default: 'a97bab29-1971-4a7f-83a8-06b20a4bd733' })
  @IsUUID()
  company: string;
}
