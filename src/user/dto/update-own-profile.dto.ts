import {
  IsBoolean,
  IsDateString,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateOwnProfileDto {
  @IsOptional()
  @IsEmail()
  secondaryEmail: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  lastName: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  pseudonym: string;

  @IsOptional()
  @ApiProperty({ default: new Date() })
  @IsDateString()
  dateOfBirth: Date;

  @IsUUID('4', { each: true })
  languages: string[];
}
