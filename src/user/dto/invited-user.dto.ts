import { IsEmail, IsUUID } from 'class-validator';

export class InvitedUserDto {
  @IsEmail()
  email: string;

  @IsUUID()
  role: string;

  @IsUUID()
  company: string;
}
