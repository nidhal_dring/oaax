import { ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDate,
  IsDateString,
  IsEnum,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
  Matches,
} from 'class-validator';
import { Transform } from 'class-transformer';
import { UserStatus } from '../enum/user-status.enum';

export class GetUserFilterDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  search: string;

  @ApiPropertyOptional({
    example: 'email:asc',
  })
  @IsString()
  @IsOptional()
  @Matches(/:(ASC|DESC)$/im)
  order: string;

  @ApiPropertyOptional({ minimum: 0, type: Number, default: 10 })
  @IsOptional()
  @IsPositive()
  @IsNumber()
  @Transform(({ value }) => parseInt(value, 10))
  limit: number;

  @ApiPropertyOptional({ minimum: 1, type: Number, default: 1 })
  @IsOptional()
  @IsNumber()
  @IsPositive()
  @Transform(({ value }) => parseInt(value, 10))
  page: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  email: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  secondaryEmail: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  fullName: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsBoolean()
  isProfileCompleted: boolean;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  firstName: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  lastName: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  pseudonym: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(UserStatus)
  status: UserStatus;

  @ApiPropertyOptional()
  @IsOptional()
  @IsDate()
  dateOfBirth: Date;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  company: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  role: string;

  @ApiPropertyOptional()
  @IsDateString()
  @IsOptional()
  createdAt: Date;
}
