import { IsNotEmpty, IsString } from 'class-validator';

export class AddUserPreferenceDto {
  @IsString()
  @IsNotEmpty()
  value: string;
}
