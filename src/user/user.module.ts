import { forwardRef, Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './repository/user.repository';
import { CompanyModule } from 'src/company/company.module';
import { InvitedUserModule } from '../invited-users/invited-user.module';
import { RoleModule } from '../role/role.module';
import { UserPreferenceModule } from '../user-preference/user-preference.module';
import { EmailModule } from '../email/email.module';
import { LanguageModule } from '../language/language.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserRepository]),
    forwardRef(() => InvitedUserModule),
    RoleModule,
    CompanyModule,
    UserPreferenceModule,
    EmailModule,
    LanguageModule,
  ],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {}
