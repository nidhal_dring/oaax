import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { Company } from '../../company/entities/company.entity';
import { Role } from '../../role/entities/role.entity';
import { Lead } from '../../lead/entities/lead.entity';
import { PinnedLead } from '../../pinned-lead/entity/pinned-lead.entity';
import { UserPreference } from '../../user-preference/entity/user-prefernce.entity';
import { UserStatus } from '../enum/user-status.enum';
import { Language } from '../../language/entities/language.entity';

// @todo: discuss if we should create interfaces for types
@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true, nullable: true })
  msAccountId: string;

  @Column({ unique: true })
  email: string;

  @Column({ nullable: true })
  secondaryEmail: string;

  @Column({ nullable: true })
  fullName: string;

  @Column({ nullable: true })
  firstName: string;

  @Column({ nullable: true })
  lastName: string;

  @Column('enum', { enum: UserStatus, default: UserStatus.PENDING })
  status: UserStatus;

  @Column({ nullable: true })
  dateOfBirth: Date;

  @Column({ nullable: true })
  pseudonym: string;

  @Column({ default: false })
  isProfileCompleted: boolean;

  @ManyToMany(() => Language, (language) => language.users)
  @JoinTable()
  languages: Language[];

  @ManyToOne(() => Company, (company) => company.users)
  company: Company;

  @ManyToOne(() => Role, (role) => role.users, { onDelete: 'CASCADE' })
  role: Role;

  @OneToMany(() => Lead, (lead) => lead.callAgent)
  leads: Lead[];

  @OneToMany(() => PinnedLead, (pinnedLead) => pinnedLead.user)
  pinnedLeads: PinnedLead[];

  @OneToMany(() => UserPreference, (userPreference) => userPreference.user)
  userPreferences: UserPreference[];

  @Exclude()
  @CreateDateColumn()
  createdAt?: string;

  @Exclude()
  @UpdateDateColumn()
  updatedAt?: string;
}
