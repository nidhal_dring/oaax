import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from './repository/user.repository';
import { User } from './entities/user.entity';
import { InvitedUserDto } from './dto/invited-user.dto';
import { CompanyService } from '../company/company.service';
import { RoleService } from '../role/role.service';
import { InvitedUserService } from '../invited-users/invited-user.service';
import { UserPreferenceService } from '../user-preference/user-preference.service';
import { UserPreference } from '../user-preference/entity/user-prefernce.entity';
import { CategoryEnum } from '../user-preference/enum/category.enum';
import { KeyEnum } from '../user-preference/enum/key.enum';
import UserPreferencesConstants from '../user-preference/consts/user-preferences.const';
import { InvitedUser } from '../invited-users/entities/invited-user.entity';
import { UpdateOwnProfileDto } from './dto/update-own-profile.dto';
import { EmailService } from '../email/email.service';
import { UserStatus } from './enum/user-status.enum';
import { GetUserFilterDto } from './dto/get-user-filter.dto';
import { Pagination } from 'nestjs-typeorm-paginate';
import { UpdateStatusDto } from './dto/update-status.dto';
import { LanguageService } from '../language/language.service';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
    private readonly companyService: CompanyService,
    private readonly roleService: RoleService,
    private readonly userPreferenceService: UserPreferenceService,
    private readonly emailService: EmailService,
    private readonly languageService: LanguageService,
    @Inject(forwardRef(() => InvitedUserService))
    private readonly registrationService: InvitedUserService,
  ) {}

  async updateUserStatus(
    id: string,
    updateStatusDto: UpdateStatusDto,
  ): Promise<User> {
    const user = await this.findOne(id);
    user.status = updateStatusDto.status;
    await this.userRepository.save(user);
    return this.findOne(id);
  }

  async inviteUser(invitedUserDto: InvitedUserDto): Promise<User> {
    await this.companyService.findOne(invitedUserDto.company);
    await this.roleService.findOne(invitedUserDto.role);
    return this.addInvitedUser(invitedUserDto);
  }

  async addInvitedUser(invitedUserDto: InvitedUserDto): Promise<User> {
    const invitedUser = await this.userRepository.addInvitedUser(
      invitedUserDto,
    );
    await this.emailService.sendInvitationEmail(invitedUser.email);
    return invitedUser;
  }

  async updateOwnProfile(
    idUser: string,
    updateOwnProfileDto: UpdateOwnProfileDto,
  ): Promise<User> {
    const user = await this.findOne(idUser);
    const languages = await this.languageService.extractLanguages(
      updateOwnProfileDto.languages,
    );

    Object.assign(user, updateOwnProfileDto);
    user.languages = languages;
    user.isProfileCompleted = true;
    await this.userRepository.save(user);

    return this.findOne(idUser);
  }

  async addUserFormMicrosoftProfile(
    user: User,
    microsoftUserProfile: any,
  ): Promise<User> {
    const updatedUser = await this.userRepository.addUserFormMicrosoftProfile(
      user,
      microsoftUserProfile,
    );
    //TODO:ADD USER PREFERENCE
    await this.userPreferenceService.createUserPreference(
      updatedUser,
      CategoryEnum.LEADS,
      KeyEnum.COLUMNS_VIEW,
      UserPreferencesConstants.leads.columnView,
    );
    await this.userPreferenceService.createUserPreference(
      updatedUser,
      CategoryEnum.GENERAL,
      KeyEnum.LANGUAGE,
      UserPreferencesConstants.general.language,
    );
    return updatedUser;
  }

  async updateUserFromMicrosoftProfile(
    userId: string,
    microsoftUserProfile: any,
  ): Promise<User> {
    await this.userRepository.updateUserFromMicrosoftProfile(
      userId,
      microsoftUserProfile,
    );
    return this.userRepository.findOne(userId, {
      relations: ['role', 'company'],
    });
  }

  async findUserByMsId(msAccountId: string): Promise<User> {
    return this.userRepository.findOne({
      where: { msAccountId },
      relations: ['role', 'company'],
    });
  }

  async resetUserPreference(
    user: User,
    category: CategoryEnum,
  ): Promise<UserPreference[]> {
    return this.userPreferenceService.resetUserPreference(user, category);
  }

  async getUserPreference(
    user: User,
    category: CategoryEnum,
    key: KeyEnum,
  ): Promise<UserPreference> {
    return this.userPreferenceService.getUserPreference(user, category, key);
  }

  async updateUserPreference(
    user: User,
    category: CategoryEnum,
    key: KeyEnum,
    value: string,
  ): Promise<UserPreference> {
    return this.userPreferenceService.updateUserPreference(
      user,
      category,
      key,
      value,
    );
  }

  findAllCallAgents(): Promise<User[]> {
    return this.userRepository.findAllCallAgents();
  }

  findAll(options: GetUserFilterDto): Promise<Pagination<User>> {
    return this.userRepository.findAllUsers(options);
  }

  findUserByEmailOrThrows(email: string): Promise<User> {
    return this.userRepository.findUserByEmailOrThrows(email);
  }

  findUserById(id: string): Promise<User> {
    return this.userRepository.findOne(id);
  }

  findOne(id: string): Promise<User> {
    return this.userRepository.findUser(id);
  }

  update(id: string, updateUserDto: UpdateUserDto): Promise<User> {
    return this.userRepository.updateUser(id, updateUserDto);
  }

  registerUser(user: User): Promise<User> {
    return this.userRepository.save(user);
  }

  remove(id: string): Promise<void> {
    return this.userRepository.deleteUser(id);
  }
}
