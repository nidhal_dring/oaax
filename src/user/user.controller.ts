import {
  Controller,
  Get,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Post,
  Put,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { ApiBearerAuth, ApiHeader, ApiTags } from '@nestjs/swagger';
import { User } from './entities/user.entity';
import { AuthGuard } from '@nestjs/passport';
import { CurrentUser } from './decorator/user.decorator';
import { UserPreference } from '../user-preference/entity/user-prefernce.entity';
import { AddUserPreferenceDto } from './dto/add-user-preference.dto';
import { CategoryEnum } from '../user-preference/enum/category.enum';
import { KeyEnum } from '../user-preference/enum/key.enum';
import { Errors } from 'src/commun/enums/Errors.enum';
import { Languages } from 'src/commun/enums/languages.enum';
import { InvitedUserDto } from './dto/invited-user.dto';
import { UpdateOwnProfileDto } from './dto/update-own-profile.dto';
import { GetUserFilterDto } from './dto/get-user-filter.dto';
import { Pagination } from 'nestjs-typeorm-paginate';
import { UpdateStatusDto } from './dto/update-status.dto';
import { Query } from '@nestjs/common';

@Controller('api/users')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@ApiTags('User')
@ApiHeader({ name: 'Accept-Language', enum: Languages })
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('user-preferences/:category/:key')
  getUserPreference(
    @CurrentUser() user: User,
    @Param('category') category: CategoryEnum,
    @Param('key') key: KeyEnum,
  ): Promise<UserPreference> {
    return this.userService.getUserPreference(user, category, key);
  }

  @Put('user-preferences/:category/:key')
  updateUserPreference(
    @CurrentUser() user: User,
    @Param('category') category: CategoryEnum,
    @Param('key') key: KeyEnum,
    @Body() addUserPreferenceDto: AddUserPreferenceDto,
  ): Promise<UserPreference> {
    return this.userService.updateUserPreference(
      user,
      category,
      key,
      addUserPreferenceDto.value,
    );
  }

  @Put('reset-user-preferences/:category')
  resetUserPreference(
    @CurrentUser() user: User,
    @Param('category') category: CategoryEnum,
  ): Promise<UserPreference[]> {
    return this.userService.resetUserPreference(user, category);
  }

  @Get()
  findAll(@Query() options: GetUserFilterDto): Promise<Pagination<User>> {
    return this.userService.findAll(options);
  }

  // @PermissionRequired(PermissionEnum.VIEW_USER, PermissionEnum.MANAGE_USER)
  @Get('/call-agents')
  findAllCallAgents(): Promise<User[]> {
    return this.userService.findAllCallAgents();
  }

  @Post('invite-user')
  inviteUser(@Body() invitedUserDto: InvitedUserDto) {
    return this.userService.inviteUser(invitedUserDto);
  }

  @Put('update-status/:id')
  updateUserStatus(
    @Param('id') id: string,
    @Body() updateStatusDto: UpdateStatusDto,
  ): Promise<User> {
    return this.userService.updateUserStatus(id, updateStatusDto);
  }

  @Get('/profile/own')
  getOwnProfile(@CurrentUser() user: User): Promise<User> {
    return this.userService.findOne(user.id);
  }

  // @PermissionRequired(PermissionEnum.VIEW_USER, PermissionEnum.MANAGE_USER)
  @Get(':id')
  findOne(@Param('id') id: string): Promise<User> {
    return this.userService.findOne(id);
  }

  // @PermissionRequired(PermissionEnum.UPDATE_USER, PermissionEnum.MANAGE_USER)
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<User> {
    return this.userService.update(id, updateUserDto);
  }

  @Put('profile')
  updateOwnProfile(
    @CurrentUser() user: User,
    @Body() updateOwnProfileDto: UpdateOwnProfileDto,
  ): Promise<User> {
    return this.userService.updateOwnProfile(user.id, updateOwnProfileDto);
  }

  // @PermissionRequired(PermissionEnum.DELETE_USER, PermissionEnum.MANAGE_USER)
  @Delete(':id')
  remove(@Param('id') id: string): Promise<void> {
    return this.userService.remove(id);
  }
}
