import { Connection, Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import users from './users.json';

export class UserSeed {
  private readonly connection: Connection;
  private readonly userRepository: Repository<User>;

  constructor(connection: Connection) {
    this.connection = connection;
    this.userRepository = connection.getRepository(User);
  }

  async run(): Promise<void> {
    await this.deleteAllUsers();
    await this.createUsers();
  }

  async deleteAllUsers(): Promise<void> {
    await this.userRepository.delete({});
    console.log('All roles delete');
  }

  async createUsers(): Promise<void> {
    await Promise.all(
      users.map((user) => {
        // we do as any as user cuz ts complains about the hashPassword function which is not in the seeds
        // see https://trello.com/c/0ETQmw8K/
        return this.userRepository.save((user as any) as User);
      }),
    );
  }
}
