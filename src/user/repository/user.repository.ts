import { EntityRepository, Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { NotFoundException } from '@nestjs/common';
import { Errors } from '../../commun/enums/Errors.enum';
import { UpdateUserDto } from '../dto/update-user.dto';
import { InvitedUserDto } from '../dto/invited-user.dto';
import { UserStatus } from '../enum/user-status.enum';
import { GetUserFilterDto } from '../dto/get-user-filter.dto';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async addInvitedUser(invitedUserDto: InvitedUserDto): Promise<User> {
    const invitedUser = new User();
    Object.assign(invitedUser, invitedUserDto);
    await this.save(invitedUser);
    return this.findOne(invitedUser.id, { relations: ['role', 'company'] });
  }

  assignUserAttributes(user: User, microsoftUserProfile: any): User {
    user.msAccountId = microsoftUserProfile._json.id;
    user.email = microsoftUserProfile._json.mail;
    user.fullName = microsoftUserProfile.displayName;
    user.firstName = microsoftUserProfile.name.givenName;
    user.lastName = microsoftUserProfile.name.familyName;
    user.pseudonym = microsoftUserProfile._json.surname;

    return user;
  }

  async addUserFormMicrosoftProfile(
    user: User,
    microsoftUserProfile: any,
  ): Promise<User> {
    user = this.assignUserAttributes(user, microsoftUserProfile);
    user.status = UserStatus.INCOMPLETE;
    await this.save(user);

    return this.findOne(user.id, { relations: ['role', 'company'] });
  }

  async updateUserFromMicrosoftProfile(
    userId: string,
    microsoftUserProfile: any,
  ): Promise<User> {
    let user = await this.findOne(userId);

    user = this.assignUserAttributes(user, microsoftUserProfile);
    await this.save(user);

    return this.findOne(user.id, { relations: ['role', 'company'] });
  }

  async findAllCallAgents(): Promise<User[]> {
    const roleName = 'CALL_AGENT';
    const query = this.createQueryBuilder('user')
      .leftJoinAndSelect('user.role', 'role')
      .select(['user.id', 'user.firstName', 'user.lastName'])
      .where('role.name = :roleName', { roleName });
    const users = await query.getMany();
    const userArray = [];
    for (const user of users) {
      const newUserFormat = { id: '', fullName: '' };
      newUserFormat.id = user.id;
      newUserFormat.fullName = user.firstName + ' ' + user.lastName;
      userArray.push(newUserFormat);
    }
    return userArray;
  }

  async findAllUsers(options: GetUserFilterDto): Promise<Pagination<User>> {
    const result = await this.createQueryBuilder('user')
      .leftJoinAndSelect('user.role', 'role')
      .leftJoinAndSelect('user.languages', 'languages')
      .leftJoinAndSelect('user.company', 'company')
      .orderBy('user.createdAt', 'DESC');
    if (options.search) {
      result.andWhere(
        `(
          LOWER(user.email) like :search OR
          LOWER(user.secondaryEmail) like :search OR
          LOWER(user.fullName) like :search OR
          LOWER(user.firstName) like :search OR
          LOWER(user.lastName) like :search OR
          LOWER(user.pseudonym) like :search OR
          LOWER(role.name) like :search OR
          LOWER(company.name) like :search
            )`,
        { search: `%${options.search.toLowerCase()}%` },
      );
    }
    if (options.email) {
      result.andWhere('LOWER(user.email) LIKE :email', {
        email: `%${options.email.toLowerCase()}%`,
      });
    }
    if (options.secondaryEmail) {
      result.andWhere('LOWER(user.secondaryEmail) LIKE :secondaryEmail', {
        secondaryEmail: `%${options.secondaryEmail.toLowerCase()}%`,
      });
    }
    if (options.fullName) {
      result.andWhere('LOWER(user.fullName) LIKE :fullName', {
        fullName: `%${options.fullName.toLowerCase()}%`,
      });
    }
    if (options.firstName) {
      result.andWhere('LOWER(user.firstName) LIKE :firstName', {
        firstName: `%${options.firstName.toLowerCase()}%`,
      });
    }
    if (options.lastName) {
      result.andWhere('LOWER(user.lastName) LIKE :lastName', {
        lastName: `%${options.lastName.toLowerCase()}%`,
      });
    }
    if (options.pseudonym) {
      result.andWhere('LOWER(user.pseudonym) LIKE :pseudonym', {
        pseudonym: `%${options.pseudonym.toLowerCase()}%`,
      });
    }

    if (options.role) {
      result.andWhere('LOWER(role.name) LIKE :role', {
        role: `%${options.role.toLowerCase()}%`,
      });
    }

    if (options.company) {
      result.andWhere('LOWER(company.name) LIKE :company', {
        company: `%${options.company.toLowerCase()}%`,
      });
    }

    if (options.status) {
      result.andWhere('user.status = :status', { status: `${options.status}` });
    }

    if (options.isProfileCompleted) {
      result.andWhere('user.isProfileCompleted = :isProfileCompleted', {
        isProfileCompleted: `${options.isProfileCompleted}`,
      });
    }

    if (options.dateOfBirth) {
      result.andWhere("TO_CHAR(user.createdAt,'YYYY-MM-DD') = :dateOfBirth", {
        dateOfBirth: `${options.dateOfBirth}`,
      });
    }

    if (options.createdAt) {
      result.andWhere("TO_CHAR(user.createdAt,'YYYY-MM-DD') = :createdAt", {
        createdAt: `${options.createdAt}`,
      });
    }

    if (options.order) {
      const args = options.order.split(':');
      const orderDirection = args[1].toUpperCase() as 'ASC' | 'DESC';
      let fieldName = args[0];
      if (!fieldName.includes('.')) {
        fieldName = `user.${fieldName}`;
      }
      result.orderBy(fieldName, orderDirection);
    }
    return paginate<User>(result, {
      limit: options.limit,
      page: options.page,
    });
  }

  async findUserByEmailOrThrows(email: string): Promise<User> {
    const user = await this.findOne(
      { email },
      { relations: ['role', 'company'] },
    );
    if (!user) {
      throw new NotFoundException(Errors.USER_NOT_FOUND);
    }
    return user;
  }

  async findUser(id: string): Promise<User> {
    const user = await this.findOne(id, {
      relations: ['role', 'company', 'languages'],
    });
    if (!user) {
      throw new NotFoundException(Errors.USER_NOT_FOUND);
    }
    return user;
  }

  async updateUser(id: string, updateUserDto: UpdateUserDto): Promise<User> {
    const user = await this.findUser(id);
    Object.assign(user, updateUserDto);
    await this.save(user);
    return this.findUser(id);
  }

  async deleteUser(id: string): Promise<void> {
    const result = await this.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(Errors.USER_NOT_FOUND);
    }
  }
}
