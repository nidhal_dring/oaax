import { FranchiseEnum } from '../enum/franchise.enum';
import {
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateInsuranceDto {
  @ApiProperty({ default: '64aab20d-bfc3-4434-a946-a1055e3de1ce' })
  @IsOptional()
  @IsUUID()
  healthInsurance: string;

  @ApiProperty({ default: 'd910286b-d71c-4868-834b-104def6cd2d1' })
  @IsOptional()
  @IsUUID()
  contractDuration: string;

  @IsOptional()
  @IsEnum(FranchiseEnum)
  franchise: FranchiseEnum;

  @ApiProperty({ default: 'fee8dc4e-663f-4377-836d-d8784225e802' })
  @IsUUID()
  hospitality: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  kkSince: string;

  @ApiProperty({ default: 'Keine' })
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  treatment: string;

  @ApiProperty({ default: 'Keine' })
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  medication: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  Last5YearAnOp: string;

  @ApiProperty({ default: 'Keine' })
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  splitting: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  kvgModel: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  monthlyPremium: string;
}
