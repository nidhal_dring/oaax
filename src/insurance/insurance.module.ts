import { Module } from '@nestjs/common';
import { InsuranceService } from './insurance.service';
import { InsuranceController } from './insurance.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InsuranceRepository } from './repository/insurance.repository';
import { ContractDurationRepository } from './repository/contarct-duration.repository';
import { HealthInsuranceRepository } from './repository/health-insurance.repository';
import { HospitalityRepository } from './repository/hospitality.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      InsuranceRepository,
      ContractDurationRepository,
      HealthInsuranceRepository,
      HospitalityRepository,
    ]),
  ],
  controllers: [InsuranceController],
  providers: [InsuranceService],
  exports: [InsuranceService],
})
export class InsuranceModule {}
