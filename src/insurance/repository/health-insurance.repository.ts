import { EntityRepository, Repository } from 'typeorm';
import { HealthInsurance } from '../entity/health-insurance.entity';
import { NotFoundException } from '@nestjs/common';
import { Errors } from '../../commun/enums/Errors.enum';

@EntityRepository(HealthInsurance)
export class HealthInsuranceRepository extends Repository<HealthInsurance> {
  async getHealthInsurance(id: string): Promise<HealthInsurance> {
    const healthInsurance = await this.findOne(id);
    if (!healthInsurance) {
      throw new NotFoundException(Errors.HEALTH_INSURANCE_NOT_FOUND);
    }
    return healthInsurance;
  }
}
