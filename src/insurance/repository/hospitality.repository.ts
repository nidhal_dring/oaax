import { EntityRepository, Repository } from 'typeorm';
import { Hospitality } from '../entity/hospitality.entity';
import { NotFoundException } from '@nestjs/common';
import { Errors } from '../../commun/enums/Errors.enum';

@EntityRepository(Hospitality)
export class HospitalityRepository extends Repository<Hospitality> {
  async getHospitality(id: string): Promise<Hospitality> {
    const hospitality = await this.findOne(id);
    if (!hospitality) {
      throw new NotFoundException(Errors.HOSPITALITY_NOT_FOUND);
    }
    return hospitality;
  }
}
