import { EntityRepository, Repository } from 'typeorm';
import { ContractDuration } from '../entity/contract-duration.entity';
import { NotFoundException } from '@nestjs/common';
import { Errors } from '../../commun/enums/Errors.enum';

@EntityRepository(ContractDuration)
export class ContractDurationRepository extends Repository<ContractDuration> {
  async getContractDuration(id: string): Promise<ContractDuration> {
    const contractDuration = await this.findOne(id);
    if (!contractDuration) {
      throw new NotFoundException(Errors.CONTRACT_DURATION_NOT_FOUND);
    }
    return contractDuration;
  }
}
