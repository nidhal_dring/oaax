import { EntityRepository, Repository } from 'typeorm';
import { Insurance } from '../entity/insurance.entity';
import { CreateInsuranceDto } from '../dto/create-insurance.dto';
import { ContractDuration } from '../entity/contract-duration.entity';
import { HealthInsurance } from '../entity/health-insurance.entity';
import { Hospitality } from '../entity/hospitality.entity';

@EntityRepository(Insurance)
export class InsuranceRepository extends Repository<Insurance> {
  async createInsurance(
    createInsuranceDto: CreateInsuranceDto,
    contractDuration: ContractDuration,
    healthInsurance: HealthInsurance,
    hospitality: Hospitality,
  ): Promise<Insurance> {
    const insurance = new Insurance();
    Object.assign(insurance, createInsuranceDto);
    insurance.healthInsurance = healthInsurance;
    insurance.contractDuration = contractDuration;
    insurance.hospitality = hospitality;
    await this.save(insurance);
    return this.findOne(insurance.id, {
      relations: ['healthInsurance', 'contractDuration', 'hospitality'],
    });
  }
}
