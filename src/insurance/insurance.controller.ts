import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { InsuranceService } from './insurance.service';
import { ApiBearerAuth, ApiHeader, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { PermissionGuard } from '../permission/guard/permission.guard';
import { PermissionRequired } from '../permission/decorator/permission.decorator';
import { PermissionEnum } from '../permission/enum/permission.enum';
import { FranchiseEnum } from './enum/franchise.enum';
import { CreateInsuranceDto } from './dto/create-insurance.dto';
import { Insurance } from './entity/insurance.entity';
import { HealthInsurance } from './entity/health-insurance.entity';
import { ContractDuration } from './entity/contract-duration.entity';
import { Hospitality } from './entity/hospitality.entity';
import { Languages } from 'src/commun/enums/languages.enum';

@Controller('api/insurances')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@ApiTags('Insurance')
@ApiHeader({ name: 'Accept-Language', enum: Languages })
export class InsuranceController {
  constructor(private readonly insuranceService: InsuranceService) {}

  // @PermissionRequired(PermissionEnum.MANAGE_INSURANCE)
  @Get('health-insurance')
  getHealthInsurance(): Promise<HealthInsurance[]> {
    return this.insuranceService.getHealthInsurance();
  }

  // @PermissionRequired(PermissionEnum.MANAGE_INSURANCE)
  @Get('contract-duration')
  getContractDuration(): Promise<ContractDuration[]> {
    return this.insuranceService.getContractDuration();
  }

  // @PermissionRequired(PermissionEnum.MANAGE_INSURANCE)
  @Get('franchise')
  getFranchise(): typeof FranchiseEnum {
    return FranchiseEnum;
  }

  // @PermissionRequired(PermissionEnum.MANAGE_INSURANCE)
  @Get('hospitality')
  getHospitality(): Promise<Hospitality[]> {
    return this.insuranceService.getHospitality();
  }

  // @PermissionRequired(PermissionEnum.MANAGE_INSURANCE)
  @Post()
  createInsurance(
    @Body() createInsuranceDto: CreateInsuranceDto,
  ): Promise<Insurance> {
    return this.insuranceService.createInsurance(createInsuranceDto);
  }
}
