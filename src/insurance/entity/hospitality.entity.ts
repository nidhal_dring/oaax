import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Insurance } from './insurance.entity';

@Entity()
export class Hospitality {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name_EN: string;

  @Column()
  name_DE: string;

  @OneToMany(() => Insurance, (insurance) => insurance.hospitality)
  insurances: Insurance[];
}
