import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Insurance } from './insurance.entity';

@Entity()
export class HealthInsurance {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @OneToMany(() => Insurance, (insurance) => insurance.healthInsurance)
  insurances: Insurance[];
}
