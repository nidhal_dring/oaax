import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { FranchiseEnum } from '../enum/franchise.enum';
import { Lead } from '../../lead/entities/lead.entity';
import { HealthInsurance } from './health-insurance.entity';
import { ContractDuration } from './contract-duration.entity';
import { Hospitality } from './hospitality.entity';
import { Exclude } from 'class-transformer';
import { Person } from 'src/lead/entities/person.entity';

@Entity()
export class Insurance {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('enum', { enum: FranchiseEnum })
  franchise: FranchiseEnum;

  @Column({ nullable: true })
  kkSince: string;

  @Column({ nullable: true })
  treatment: string;

  @Column({ nullable: true })
  medication: string;

  @Column({ nullable: true })
  Last5YearAnOp: string;

  @Column({ nullable: true })
  splitting: string;

  @Column({ nullable: true })
  kvgModel: string;

  @Column({ nullable: true })
  monthlyPremium: string;

  @OneToMany(() => Person, (person) => person.insurance)
  persons: Person[];

  @ManyToOne(
    () => HealthInsurance,
    (healthInsurance) => healthInsurance.insurances,
  )
  healthInsurance: HealthInsurance;

  @ManyToOne(
    () => ContractDuration,
    (contractDuration) => contractDuration.insurances,
  )
  contractDuration: ContractDuration;

  @ManyToOne(() => Hospitality, (hospitality) => hospitality.insurances)
  hospitality: Hospitality;

  @Exclude()
  @CreateDateColumn()
  createdAt: string;

  @Exclude()
  @UpdateDateColumn()
  updatedAt: string;
}
