import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Insurance } from './insurance.entity';

@Entity()
export class ContractDuration {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name_EN: string;

  @Column()
  name_DE: string;

  @OneToMany(() => Insurance, (insurance) => insurance.contractDuration)
  insurances: Insurance[];
}
