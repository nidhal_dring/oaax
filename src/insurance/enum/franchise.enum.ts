export enum FranchiseEnum {
  CHF_0 = '0 Chf.',
  CHF_100 = '100 Chf.',
  CHF_200 = '200 Chf.',
  CHF_300 = '300 Chf.',
  CHF_400 = '400 Chf.',
  CHF_500 = '500 Chf.',
  CHF_600 = '600 Chf.',
  CHF_700 = '700 Chf.',
  CHF_800 = '800 Chf.',
  CHF_900 = '900 Chf.',
  CHF_1000 = '1000 Chf.',
  CHF_1100 = '1100 Chf.',
  CHF_1200 = '1200 Chf.',
  CHF_1300 = '1300 Chf.',
  CHF_1400 = '1400 Chf.',
  CHF_1500 = '1500 Chf.',
  CHF_1600 = '1600 Chf.',
  CHF_1700 = '1700 Chf.',
  CHF_1800 = '1800 Chf.',
  CHF_1900 = '1900 Chf.',
  CHF_2000 = '2000 Chf.',
  CHF_2100 = '2100 Chf.',
  CHF_2200 = '2200 Chf.',
  CHF_2300 = '2300 Chf.',
  CHF_2400 = '2400 Chf.',
  CHF_2500 = '2500 Chf.',
}
