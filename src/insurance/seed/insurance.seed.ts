import { Connection, Repository } from 'typeorm';
import { ContractDuration } from '../entity/contract-duration.entity';
import { HealthInsurance } from '../entity/health-insurance.entity';
import { Hospitality } from '../entity/hospitality.entity';
import healthInsurances from './health-insurances.json';
import contractDurations from './contract-duration.json';
import hospitalizes from './hospitality.json';

export class InsuranceSeed {
  private readonly connection: Connection;
  private readonly contractDurationRepository: Repository<ContractDuration>;
  private readonly healthInsuranceRepository: Repository<HealthInsurance>;
  private readonly hospitalityRepository: Repository<Hospitality>;

  constructor(connection: Connection) {
    this.connection = connection;
    this.contractDurationRepository = connection.getRepository(
      ContractDuration,
    );
    this.healthInsuranceRepository = connection.getRepository(HealthInsurance);
    this.hospitalityRepository = connection.getRepository(Hospitality);
  }

  async run(): Promise<void> {
    await this.deleteAllContractDuration();
    await this.deleteAllHealthInsurance();
    await this.deleteAllHospitality();
    await this.createContractDuration();
    await this.createHealthInsurance();
    await this.createHospitality();
  }

  async deleteAllContractDuration(): Promise<void> {
    await this.contractDurationRepository.delete({});
  }

  async deleteAllHealthInsurance(): Promise<void> {
    await this.healthInsuranceRepository.delete({});
  }

  async deleteAllHospitality(): Promise<void> {
    await this.hospitalityRepository.delete({});
  }

  async createHealthInsurance(): Promise<void> {
    await Promise.all(
      healthInsurances.map((healthInsurance) => {
        this.healthInsuranceRepository.save(healthInsurance as HealthInsurance);
      }),
    );
  }

  async createContractDuration(): Promise<void> {
    await Promise.all(
      contractDurations.map((contractDuration) => {
        this.contractDurationRepository.save(
          contractDuration as ContractDuration,
        );
      }),
    );
  }

  async createHospitality(): Promise<void> {
    await Promise.all(
      hospitalizes.map((hospitality) => {
        this.hospitalityRepository.save(hospitality as Hospitality);
      }),
    );
  }
}
