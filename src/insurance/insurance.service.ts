import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ContractDurationRepository } from './repository/contarct-duration.repository';
import { HealthInsuranceRepository } from './repository/health-insurance.repository';
import { HospitalityRepository } from './repository/hospitality.repository';
import { ContractDuration } from './entity/contract-duration.entity';
import { HealthInsurance } from './entity/health-insurance.entity';
import { CreateInsuranceDto } from './dto/create-insurance.dto';
import { Insurance } from './entity/insurance.entity';
import { InsuranceRepository } from './repository/insurance.repository';

@Injectable()
export class InsuranceService {
  constructor(
    @InjectRepository(InsuranceRepository)
    private readonly insuranceRepository: InsuranceRepository,
    @InjectRepository(ContractDurationRepository)
    private readonly contractDurationRepository: ContractDurationRepository,
    @InjectRepository(HealthInsuranceRepository)
    private readonly healthInsuranceRepository: HealthInsuranceRepository,
    @InjectRepository(HospitalityRepository)
    private readonly hospitalityRepository: HospitalityRepository,
  ) {}

  getContractDuration(): Promise<ContractDuration[]> {
    return this.contractDurationRepository.find();
  }

  getHealthInsurance(): Promise<HealthInsurance[]> {
    return this.healthInsuranceRepository.find();
  }

  getHospitality(): Promise<ContractDuration[]> {
    return this.hospitalityRepository.find();
  }

  async createInsurance(
    createInsuranceDto: CreateInsuranceDto,
  ): Promise<Insurance> {
    const {
      healthInsurance,
      contractDuration,
      hospitality,
    } = createInsuranceDto;
    const contractDurationEntity = await this.contractDurationRepository.getContractDuration(
      contractDuration,
    );
    const healthInsuranceEntity = await this.healthInsuranceRepository.getHealthInsurance(
      healthInsurance,
    );
    const hospitalityEntity = await this.hospitalityRepository.getHospitality(
      hospitality,
    );
    return this.insuranceRepository.createInsurance(
      createInsuranceDto,
      contractDurationEntity,
      healthInsuranceEntity,
      hospitalityEntity,
    );
  }
}
