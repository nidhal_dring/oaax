import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { configService } from 'src/config/config.service';
import { EmailService } from './email.service';

@Module({
  imports: [MailerModule.forRoot(configService.getMailConfig())],
  providers: [EmailService],
  exports: [EmailService],
})
export class EmailModule {}
