import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { User } from 'src/user/entities/user.entity';

@Injectable()
export class EmailService {
  constructor(private readonly nodeMailerService: MailerService) {}

  async sendConfirmationEmail(user: User): Promise<void> {
    return this.nodeMailerService.sendMail({
      to: user.email,
      from: {
        name: 'Oaax Registartion Team',
        address: 'oaaxmailer@gmail.com',
      },
      subject: 'CONFIRMATION',
      template: './confirmation',
      context: {
        ...user,
      },
    });
  }

  async sendInvitationEmail(email: string): Promise<void> {
    return this.nodeMailerService.sendMail({
      to: email,
      from: {
        name: 'Oaax Registartion Team',
        address: 'oaaxmailer@gmail.com',
      },
      subject: 'Registration',
      template: './registration',
    });
  }
}
