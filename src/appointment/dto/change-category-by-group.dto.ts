import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsEnum, IsUUID, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { AppointmentCategoryEnum } from '../enum/appointment-category.enum';

class AppointmentDTO {
  @ApiProperty({
    required: true,
    default: 'a97bab29-1971-4a7f-83a8-06b20a4bd733',
  })
  @IsUUID()
  id: string;
}

export class ChangeCategoryByGroupDto {
  @IsEnum(AppointmentCategoryEnum)
  category: AppointmentCategoryEnum;

  @ValidateNested({ each: true })
  @IsArray()
  @Type(() => AppointmentDTO)
  appointments: AppointmentDTO[];
}
