import { IsUUID } from 'class-validator';

export class AssignToAgentDto {
  @IsUUID()
  agent: string;
}
