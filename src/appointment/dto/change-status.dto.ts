import { IsEnum } from 'class-validator';
import { AppointmentStatusEnum } from '../enum/appointment-status.enum';

export class ChangeStatusDto {
  @IsEnum(AppointmentStatusEnum)
  status: AppointmentStatusEnum;
}
