import { IsEnum } from 'class-validator';
import { AppointmentCategoryEnum } from '../enum/appointment-category.enum';

export class ChangeCategoryDto {
  @IsEnum(AppointmentCategoryEnum)
  category: AppointmentCategoryEnum;
}
