import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsEnum, IsUUID } from 'class-validator';
import { AppointmentCategoryEnum } from '../enum/appointment-category.enum';

export class CreateAppointmentDto {
  @ApiProperty({ required: true, default: new Date().toISOString() })
  @IsDateString()
  appointmentDate: Date;

  @ApiProperty({ default: AppointmentCategoryEnum.RECALL })
  @IsEnum(AppointmentCategoryEnum)
  category: AppointmentCategoryEnum;

  @ApiProperty({ default: '3291b4e4-2c8c-45ef-b176-5c0070a1ec53' })
  @IsUUID()
  language: string;
}
