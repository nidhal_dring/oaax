import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsEnum, IsOptional, IsUUID } from 'class-validator';
import { AppointmentCategoryEnum } from '../enum/appointment-category.enum';
import { AppointmentStatusEnum } from '../enum/appointment-status.enum';

export class UpdateAppointmentDto {
  @ApiProperty({ required: true, default: new Date().toISOString() })
  @IsDateString()
  appointmentDate: Date;

  @ApiProperty({ default: AppointmentCategoryEnum.RECALL })
  @IsEnum(AppointmentCategoryEnum)
  category: AppointmentCategoryEnum;

  @ApiProperty({ default: 'a97bab29-1971-4a7f-83a8-06b20a4bd733' })
  @IsUUID()
  language: string;

  @ApiProperty({ default: AppointmentStatusEnum.CREATED })
  @IsEnum(AppointmentStatusEnum)
  @IsOptional()
  status: AppointmentStatusEnum;
}
