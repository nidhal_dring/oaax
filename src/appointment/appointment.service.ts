import { ConflictException, Injectable } from '@nestjs/common';
import { CreateAppointmentDto } from './dto/create-appointment.dto';
import { UpdateAppointmentDto } from './dto/update-appointment.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { AppointmentRepository } from './repository/appointment.repository';
import { Appointment } from './entities/appointment.entity';
import { LanguageService } from '../language/language.service';
import { AssignToAgentDto } from './dto/assign-to-agent.dto';
import { ChangeStatusDto } from './dto/change-status.dto';
import { AppointmentStatusEnum } from './enum/appointment-status.enum';
import { Errors } from '../commun/enums/Errors.enum';
import { ChangeCategoryDto } from './dto/change-category.dto';
import { AppointmentCategoryEnum } from './enum/appointment-category.enum';
import { ChangeStatusByGroupDto } from './dto/change-status-by-group.dto';
import { ChangeCategoryByGroupDto } from './dto/change-category-by-group.dto';

@Injectable()
export class AppointmentService {
  constructor(
    @InjectRepository(AppointmentRepository)
    private readonly appointmentRepository: AppointmentRepository,
    private readonly languageService: LanguageService,
  ) {}

  // async assignToAgent(
  //   id: string,
  //   assignToAgentDto: AssignToAgentDto,
  // ): Promise<Appointment> {
  //   return this.appointmentRepository.assignToAgent(id, agent);
  // }

  async create(
    createAppointmentDto: CreateAppointmentDto,
  ): Promise<Appointment> {
    if (createAppointmentDto.language) {
      await this.languageService.findOne(createAppointmentDto.language);
    }
    return this.appointmentRepository.createAppointment(createAppointmentDto);
  }

  findAll(): Promise<Appointment[]> {
    return this.appointmentRepository.findAllAppointment();
  }

  findOne(id: string): Promise<Appointment> {
    return this.appointmentRepository.findAppointmentById(id);
  }

  async update(
    id: string,
    updateAppointmentDto: UpdateAppointmentDto,
  ): Promise<Appointment> {
    const { language } = updateAppointmentDto;

    if (language) {
      await this.languageService.findOne(language);
    }
    return this.appointmentRepository.updateAppointment(
      id,
      updateAppointmentDto,
    );
  }

  remove(id: string): Promise<void> {
    return this.appointmentRepository.deleteAppointment(id);
  }

  async changeAppointmentStatusByGroup(
    changeStatusByGroupDto: ChangeStatusByGroupDto,
  ): Promise<void> {
    const { appointments, status } = changeStatusByGroupDto;
    const appointmentArray = [];
    for (const appointment of appointments) {
      const appointmentEntity = await this.findOne(appointment.id);
      appointmentEntity.status = this.handleAppointmentStatus(
        appointmentEntity.status,
        status,
      );
      appointmentArray.push(appointmentEntity);
    }
    await this.appointmentRepository.save(appointmentArray);
  }

  async changeAppointmentCategoryByGroup(
    changeCategoryByGroupDto: ChangeCategoryByGroupDto,
  ): Promise<void> {
    const { appointments, category } = changeCategoryByGroupDto;
    const appointmentArray = [];
    for (const appointment of appointments) {
      const appointmentEntity = await this.findOne(appointment.id);
      appointmentEntity.category = this.handleAppointmentCategory(
        appointmentEntity.category,
        category,
      );
      appointmentArray.push(appointmentEntity);
    }
    await this.appointmentRepository.save(appointmentArray);
  }

  async changeAppointmentStatus(
    id: string,
    changeStatusDto: ChangeStatusDto,
  ): Promise<void> {
    const appointment = await this.findOne(id);
    appointment.status = this.handleAppointmentStatus(
      appointment.status,
      changeStatusDto.status,
    );
    await this.appointmentRepository.save(appointment);
  }

  async changeAppointmentCategory(
    id: string,
    changeCategoryDto: ChangeCategoryDto,
  ): Promise<void> {
    const appointment = await this.findOne(id);
    appointment.category = this.handleAppointmentCategory(
      appointment.category,
      changeCategoryDto.category,
    );
    await this.appointmentRepository.save(appointment);
  }

  handleAppointmentStatus(
    currentStatus: AppointmentStatusEnum,
    newStatus: AppointmentStatusEnum,
  ): AppointmentStatusEnum {
    if (
      currentStatus === AppointmentStatusEnum.CREATED &&
      newStatus !== AppointmentStatusEnum.CREATED
    ) {
      return newStatus;
    } else if (
      currentStatus === AppointmentStatusEnum.CHECKED &&
      (newStatus === AppointmentStatusEnum.REJECTED ||
        newStatus === AppointmentStatusEnum.ACCEPTED)
    ) {
      return newStatus;
    } else if (
      currentStatus === AppointmentStatusEnum.ACCEPTED &&
      (newStatus === AppointmentStatusEnum.REJECTED ||
        newStatus === AppointmentStatusEnum.CHECKED)
    ) {
      return newStatus;
    } else if (
      currentStatus === AppointmentStatusEnum.REJECTED &&
      (newStatus === AppointmentStatusEnum.ACCEPTED ||
        newStatus === AppointmentStatusEnum.CHECKED)
    ) {
      return newStatus;
    } else {
      throw new ConflictException(Errors.WRONG_APPOINTMENT_STATUS);
    }
  }

  handleAppointmentCategory(
    currenctCatgory: AppointmentCategoryEnum,
    newCategory: AppointmentCategoryEnum,
  ): AppointmentCategoryEnum {
    if (currenctCatgory === newCategory) {
      throw new ConflictException(Errors.WRONG_APPOINTMENT_CATEGORY);
    }
    return newCategory;
  }
}
