import { Module } from '@nestjs/common';
import { AppointmentService } from './appointment.service';
import { AppointmentController } from './appointment.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppointmentRepository } from './repository/appointment.repository';
import { LanguageModule } from '../language/language.module';

@Module({
  imports: [TypeOrmModule.forFeature([AppointmentRepository]), LanguageModule],
  controllers: [AppointmentController],
  providers: [AppointmentService],
  exports: [AppointmentService],
})
export class AppointmentModule {}
