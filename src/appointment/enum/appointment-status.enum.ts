export enum AppointmentStatusEnum {
  CREATED = 'Created',
  ACCEPTED = 'Accepted',
  CHECKED = 'Checked',
  REJECTED = 'Rejected',
}
