import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AppointmentService } from './appointment.service';
import { CreateAppointmentDto } from './dto/create-appointment.dto';
import { UpdateAppointmentDto } from './dto/update-appointment.dto';
import { ApiBearerAuth, ApiHeader, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { PermissionGuard } from '../permission/guard/permission.guard';
import { PermissionRequired } from '../permission/decorator/permission.decorator';
import { PermissionEnum } from '../permission/enum/permission.enum';
import { Appointment } from './entities/appointment.entity';
import { AssignToAgentDto } from './dto/assign-to-agent.dto';
import { ChangeStatusDto } from './dto/change-status.dto';
import { ChangeCategoryDto } from './dto/change-category.dto';
import { ChangeStatusByGroupDto } from './dto/change-status-by-group.dto';
import { ChangeCategoryByGroupDto } from './dto/change-category-by-group.dto';
import { Languages } from 'src/commun/enums/languages.enum';

@Controller('api/appointments')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@ApiTags('Appointment')
@ApiHeader({ name: 'Accept-Language', enum: Languages })
export class AppointmentController {
  constructor(private readonly appointmentService: AppointmentService) {}

  // @PermissionRequired(
  //   PermissionEnum.CREATE_APPOINTMENT,
  //   PermissionEnum.MANAGE_APPOINTMENT,
  // )
  @Post()
  create(
    @Body() createAppointmentDto: CreateAppointmentDto,
  ): Promise<Appointment> {
    return this.appointmentService.create(createAppointmentDto);
  }

  // @PermissionRequired(
  //   PermissionEnum.VIEW_APPOINTMENT,
  //   PermissionEnum.MANAGE_APPOINTMENT,
  // )
  @Get()
  findAll(): Promise<Appointment[]> {
    return this.appointmentService.findAll();
  }

  // @PermissionRequired(
  //   PermissionEnum.VIEW_APPOINTMENT,
  //   PermissionEnum.MANAGE_APPOINTMENT,
  // )
  @Get(':id')
  findOne(@Param('id') id: string): Promise<Appointment> {
    return this.appointmentService.findOne(id);
  }

  // @PermissionRequired(
  //   PermissionEnum.UPDATE_APPOINTMENT,
  //   PermissionEnum.MANAGE_APPOINTMENT,
  // )
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateAppointmentDto: UpdateAppointmentDto,
  ): Promise<Appointment> {
    return this.appointmentService.update(id, updateAppointmentDto);
  }

  // @PermissionRequired(
  //   PermissionEnum.CREATE_APPOINTMENT,
  //   PermissionEnum.MANAGE_APPOINTMENT,
  // )
  @Delete(':id')
  remove(@Param('id') id: string): Promise<void> {
    return this.appointmentService.remove(id);
  }

  // @PermissionRequired(
  //   PermissionEnum.UPDATE_APPOINTMENT,
  //   PermissionEnum.MANAGE_APPOINTMENT,
  // )
  @Put(':id/change-status')
  async changeAppointmentStatus(
    @Param('id') id: string,
    @Body() changeStatusDto: ChangeStatusDto,
  ): Promise<void> {
    await this.appointmentService.changeAppointmentStatus(id, changeStatusDto);
  }

  // @PermissionRequired(
  //   PermissionEnum.UPDATE_APPOINTMENT,
  //   PermissionEnum.MANAGE_APPOINTMENT,
  // )
  @Put('change-status')
  async changeAppointmentStatusByGroup(
    @Body() changeStatusByGroupDto: ChangeStatusByGroupDto,
  ): Promise<void> {
    await this.appointmentService.changeAppointmentStatusByGroup(
      changeStatusByGroupDto,
    );
  }

  // @PermissionRequired(
  //   PermissionEnum.UPDATE_APPOINTMENT,
  //   PermissionEnum.MANAGE_APPOINTMENT,
  // )
  @Put(':id/change-category')
  async changeAppointmentCategory(
    @Param('id') id: string,
    @Body() changeCategoryDto: ChangeCategoryDto,
  ): Promise<void> {
    await this.appointmentService.changeAppointmentCategory(
      id,
      changeCategoryDto,
    );
  }

  // @PermissionRequired(
  //   PermissionEnum.UPDATE_APPOINTMENT,
  //   PermissionEnum.MANAGE_APPOINTMENT,
  // )
  @Put('change-category')
  async changeAppointmentCategoryByGroup(
    @Body() changeCategoryByGroupDto: ChangeCategoryByGroupDto,
  ): Promise<void> {
    await this.appointmentService.changeAppointmentCategoryByGroup(
      changeCategoryByGroupDto,
    );
  }
}
