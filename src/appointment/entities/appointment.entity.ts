import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { Language } from '../../language/entities/language.entity';
import { AppointmentCategoryEnum } from '../enum/appointment-category.enum';
import { AppointmentStatusEnum } from '../enum/appointment-status.enum';
import { Lead } from '../../lead/entities/lead.entity';

@Entity()
export class Appointment {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  appointmentDate: Date;

  @Column('enum', { enum: AppointmentCategoryEnum })
  category: AppointmentCategoryEnum;

  @Column('enum', {
    enum: AppointmentStatusEnum,
    default: AppointmentStatusEnum.CREATED,
  })
  status: AppointmentStatusEnum;

  @ManyToOne(() => Language, (language) => language.appointments)
  language: Language;

  @ManyToOne(() => Lead, (lead) => lead.appointments)
  lead: Lead;

  @Exclude()
  @CreateDateColumn()
  createdAt: string;

  @Exclude()
  @UpdateDateColumn()
  updatedAt: string;
}
