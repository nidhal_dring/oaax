import { EntityRepository, Repository } from 'typeorm';
import { Appointment } from '../entities/appointment.entity';
import { CreateAppointmentDto } from '../dto/create-appointment.dto';
import { NotFoundException } from '@nestjs/common';
import { Errors } from '../../commun/enums/Errors.enum';
import { UpdateAppointmentDto } from '../dto/update-appointment.dto';

@EntityRepository(Appointment)
export class AppointmentRepository extends Repository<Appointment> {
  async createAppointment(
    createAppointmentDto: CreateAppointmentDto,
  ): Promise<Appointment> {
    const appointment = new Appointment();
    Object.assign(appointment, createAppointmentDto);
    await this.save(appointment);
    return this.findOne(appointment.id, {
      relations: ['language', 'lead'],
    });
  }

  async findAllAppointment(): Promise<Appointment[]> {
    return this.find({ relations: ['language', 'lead'] });
  }

  async findAppointmentById(id: string): Promise<Appointment> {
    const appointment = await this.findOne(id, {
      relations: ['language', 'lead'],
    });
    if (!appointment) {
      throw new NotFoundException(Errors.APPOINTMENT_NOT_FOUND);
    }
    return appointment;
  }

  async updateAppointment(
    id: string,
    updateAppointmentDto: UpdateAppointmentDto,
  ): Promise<Appointment> {
    const appointment = await this.findAppointmentById(id);
    Object.assign(appointment, updateAppointmentDto);
    await this.save(appointment);
    return this.findAppointmentById(id);
  }

  async deleteAppointment(id: string): Promise<void> {
    const result = await this.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(Errors.APPOINTMENT_NOT_FOUND);
    }
  }
}
