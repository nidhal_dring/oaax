import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import path from 'path';
import dotenv from 'dotenv';
import { MailerOptions } from '@nestjs-modules/mailer';
import { join } from 'path';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';

dotenv.config({
  path: path.resolve(process.cwd(), `.env.${process.env.NODE_ENV || 'local'}`),
});

class ConfigService {
  constructor(private env: { [k: string]: string | undefined }) {}

  private getValue(key: string, throwOnMissing = true): string {
    const value = this.env[key];
    if (!value && throwOnMissing) {
      throw new Error(`config error - missing env.${key}`);
    }

    return value;
  }

  public ensureValues(keys: string[]) {
    keys.forEach((k) => this.getValue(k, true));
    return this;
  }

  public getPort() {
    return this.getValue('PORT', true);
  }

  public isProduction() {
    const mode = this.getValue('MODE', false);
    return mode !== 'DEV';
  }

  public getTypeOrmConfig(): TypeOrmModuleOptions {
    return {
      type: 'postgres',

      host: this.getValue('DB_HOST'),
      port: parseInt(this.getValue('DB_PORT')),
      username: this.getValue('DB_USER'),
      password: this.getValue('DB_PASSWORD'),
      database: this.getValue('DB_DATABASE'),
      synchronize: JSON.parse(this.getValue('RUN_MIGRATIONS').toLowerCase()),
      logging: JSON.parse(this.getValue('RUN_LOGGER').toLowerCase()),
      entities: [`${__dirname}/../**/*.entity.{ts,js}`],

      migrationsTableName: 'migration',

      migrations: ['apps/template-nest/src/migration/*.ts'],

      cli: {
        migrationsDir: 'apps/template-nest/src/migration',
      },

      ssl: this.isProduction(),
    };
  }

  public getMicrosoftAuthConfig() {
    return {
      CLIENT_ID: this.getValue('CLIENT_ID'),
      CLIENT_SECRET: this.getValue('CLIENT_SECRET'),
      CALLBACK_URL: this.getValue('CALLBACK_URL'),
      SCOPE: this.getValue('SCOPE'),
    };
  }

  public getJWTConfig() {
    return {
      JWT_EXPIRATION: this.getValue('JWT_EXPIRATION'),
      JWT_SECRET: this.getValue('JWT_SECRET'),
    };
  }

  public getMailConfig(): MailerOptions {
    return {
      transport: {
        service: this.getValue('NODEMAILER_TRANSPORT_SERVICE'),
        auth: {
          user: this.getValue('NODEMAILER_EMAIL'),
          pass: this.getValue('NODEMAILER_PASS'),
        },
      },
      template: {
        dir: join(__dirname, '..', 'email', 'templates'),
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
      defaults: {
        from: this.getValue('NODEMAILER_DEFAULT_FROM'),
      },
    };
  }
}

const configService = new ConfigService(process.env).ensureValues([
  'DB_HOST',
  'DB_PORT',
  'DB_USER',
  'DB_PASSWORD',
  'DB_DATABASE',
  'RUN_LOGGER',
  'RUN_MIGRATIONS',
  'JWT_SECRET',
  'JWT_EXPIRATION',
  'NODEMAILER_TRANSPORT_SERVICE',
  'NODEMAILER_EMAIL',
  'NODEMAILER_PASS',
  'NODEMAILER_DEFAULT_FROM',
  'CLIENT_ID',
  'CLIENT_SECRET',
  'CALLBACK_URL',
  'SCOPE',
]);

export { configService };
