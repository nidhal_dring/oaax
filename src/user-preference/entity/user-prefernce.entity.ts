import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../../user/entities/user.entity';
import { CategoryEnum } from '../enum/category.enum';
import { KeyEnum } from '../enum/key.enum';

@Entity()
export class UserPreference {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => User, (user) => user.userPreferences)
  user: User;

  @Column('enum', { enum: CategoryEnum })
  category: CategoryEnum;

  @Column('enum', { enum: KeyEnum })
  key: KeyEnum;

  @Column()
  value: string;
}
