import { Module } from '@nestjs/common';
import { UserPreferenceService } from './user-preference.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserPreferenceRepository } from './repository/user-preference.repository';

@Module({
  imports: [TypeOrmModule.forFeature([UserPreferenceRepository])],
  providers: [UserPreferenceService],
  exports: [UserPreferenceService],
})
export class UserPreferenceModule {}
