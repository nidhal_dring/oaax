export default {
  leads: {
    columnView:
      'createdAt,fullName,phoneNumber,dateOfBirth,plz,place,languages,category,status,gender,callAgent',
  },
  general: {
    language: 'en',
  },
};
