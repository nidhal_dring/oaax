import { EntityRepository, Repository } from 'typeorm';
import { UserPreference } from '../entity/user-prefernce.entity';
import { NotFoundException } from '@nestjs/common';
import { Errors } from '../../commun/enums/Errors.enum';
import { User } from '../../user/entities/user.entity';
import { CategoryEnum } from '../enum/category.enum';
import { KeyEnum } from '../enum/key.enum';

@EntityRepository(UserPreference)
export class UserPreferenceRepository extends Repository<UserPreference> {
  async getUserPreference(
    user: User,
    category: CategoryEnum,
    key: KeyEnum,
  ): Promise<UserPreference> {
    const query = this.createQueryBuilder('user_preference')
      .leftJoin('user_preference.user', 'user')
      .where('user.id = :userId', { userId: user.id })
      .andWhere('key = :key', { key })
      .andWhere('category = :category', { category });
    const result = await query.getOne();
    if (!result) {
      throw new NotFoundException(Errors.USER_PREFERENCE_NOT_FOUND);
    }
    return result;
  }
}
