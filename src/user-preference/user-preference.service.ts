import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserPreferenceRepository } from './repository/user-preference.repository';
import { User } from '../user/entities/user.entity';
import { UserPreference } from './entity/user-prefernce.entity';
import { CategoryEnum } from './enum/category.enum';
import { KeyEnum } from './enum/key.enum';
import UserPreferencesConstants from './consts/user-preferences.const';

@Injectable()
export class UserPreferenceService {
  constructor(
    @InjectRepository(UserPreferenceRepository)
    private readonly userPreferenceRepository: UserPreferenceRepository,
  ) {}

  async resetUserPreference(
    user: User,
    category: CategoryEnum,
  ): Promise<UserPreference[]> {
    switch (category) {
      case CategoryEnum.LEADS:
        const userPreference = await this.getUserPreference(
          user,
          category,
          KeyEnum.COLUMNS_VIEW,
        );
        userPreference.value = UserPreferencesConstants.leads.columnView;
        await this.userPreferenceRepository.save(userPreference);
        return this.userPreferenceRepository.find({
          where: {
            category,
          },
        });
    }
  }

  async getUserPreference(
    user: User,
    category: CategoryEnum,
    key: KeyEnum,
  ): Promise<UserPreference> {
    return this.userPreferenceRepository.getUserPreference(user, category, key);
  }

  async updateUserPreference(
    user: User,
    category: CategoryEnum,
    key: KeyEnum,
    value: string,
  ): Promise<UserPreference> {
    const userPreference = await this.getUserPreference(user, category, key);
    userPreference.value = value;
    return this.userPreferenceRepository.save(userPreference);
  }

  async createUserPreference(
    user: User,
    category: CategoryEnum,
    key: KeyEnum,
    value: string,
  ): Promise<UserPreference> {
    const userPreference = new UserPreference();
    userPreference.user = user;
    userPreference.category = category;
    userPreference.key = key;
    userPreference.value = value;
    return this.userPreferenceRepository.save(userPreference);
  }
}
