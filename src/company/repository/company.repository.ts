import { EntityRepository, Repository } from 'typeorm';
import { Company } from '../entities/company.entity';
import { CreateCompanyDto } from '../dto/create-company.dto';
import { NotFoundException } from '@nestjs/common';
import { Errors } from '../../commun/enums/Errors.enum';
import { UpdateCompanyDto } from '../dto/update-company.dto';

@EntityRepository(Company)
export class CompanyRepository extends Repository<Company> {
  async createCompany(createCompanyDto: CreateCompanyDto): Promise<Company> {
    const company = new Company(createCompanyDto.name);
    return this.save(company);
  }

  async findAllCompanies(): Promise<Company[]> {
    return this.find();
  }

  async findCompany(id: string): Promise<Company> {
    const company = await this.findOne(id);
    if (!company) {
      throw new NotFoundException(Errors.COMPANY_NOT_FOUND);
    }
    return company;
  }

  async updateCompany(
    id: string,
    updateCompanyDto: UpdateCompanyDto,
  ): Promise<Company> {
    const company = await this.findCompany(id);
    Object.assign(company, updateCompanyDto);
    return this.save(company);
  }

  async deleteCompany(id: string): Promise<void> {
    const result = await this.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(Errors.COMPANY_NOT_FOUND);
    }
  }
}
