import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { User } from '../../user/entities/user.entity';
import { InvitedUser } from '../../invited-users/entities/invited-user.entity';

@Entity()
export class Company {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @OneToMany(() => User, (user) => user.company, { nullable: true })
  users: User[];

  @OneToMany(() => InvitedUser, (invitedUser) => invitedUser.company)
  invitedUsers: InvitedUser[];

  @Exclude()
  @CreateDateColumn()
  createdAt: string;

  @Exclude()
  @UpdateDateColumn()
  updatedAt: string;

  constructor(name?: string) {
    this.name = name;
  }
}
