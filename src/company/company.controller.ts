import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { CompanyService } from './company.service';
import { CreateCompanyDto } from './dto/create-company.dto';
import { UpdateCompanyDto } from './dto/update-company.dto';
import { ApiBearerAuth, ApiHeader, ApiTags } from '@nestjs/swagger';
import { ResponseCompanyDto } from './dto/response.company.dto';
import { AuthGuard } from '@nestjs/passport';
import { PermissionGuard } from '../permission/guard/permission.guard';
import { PermissionRequired } from '../permission/decorator/permission.decorator';
import { PermissionEnum } from '../permission/enum/permission.enum';
import { Languages } from 'src/commun/enums/languages.enum';

@Controller('api/company')
@ApiTags('Company')
@ApiHeader({ name: 'Accept-Language', enum: Languages })
export class CompanyController {
  constructor(private readonly companyService: CompanyService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  // @PermissionRequired(
  //   PermissionEnum.CREATE_COMPANY,
  //   PermissionEnum.MANAGE_COMPANY,
  // )
  create(
    @Body() createCompanyDto: CreateCompanyDto,
  ): Promise<ResponseCompanyDto> {
    return this.companyService.createCompany(createCompanyDto);
  }

  @Get()
  findAll(): Promise<ResponseCompanyDto[]> {
    return this.companyService.findAll();
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  // @PermissionRequired(
  //   PermissionEnum.VIEW_COMPANY,
  //   PermissionEnum.MANAGE_COMPANY,
  // )
  findOne(@Param('id') id: string): Promise<ResponseCompanyDto> {
    return this.companyService.findOne(id);
  }

  @Patch(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  // @PermissionRequired(
  //   PermissionEnum.UPDATE_LANGUAGES,
  //   PermissionEnum.MANAGE_COMPANY,
  // )
  update(
    @Param('id') id: string,
    @Body() updateCompanyDto: UpdateCompanyDto,
  ): Promise<ResponseCompanyDto> {
    return this.companyService.update(id, updateCompanyDto);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  // @PermissionRequired(
  //   PermissionEnum.DELETE_COMPANY,
  //   PermissionEnum.MANAGE_COMPANY,
  // )
  remove(@Param('id') id: string): Promise<void> {
    return this.companyService.remove(id);
  }
}
