import { Injectable } from '@nestjs/common';
import { CreateCompanyDto } from './dto/create-company.dto';
import { UpdateCompanyDto } from './dto/update-company.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CompanyRepository } from './repository/company.repository';
import { Company } from './entities/company.entity';

@Injectable()
export class CompanyService {
  constructor(
    @InjectRepository(CompanyRepository)
    private readonly companyRepository: CompanyRepository,
  ) {}

  createCompany(createCompanyDto: CreateCompanyDto): Promise<Company> {
    return this.companyRepository.createCompany(createCompanyDto);
  }

  findAll(): Promise<Company[]> {
    return this.companyRepository.findAllCompanies();
  }

  findOne(id: string): Promise<Company> {
    return this.companyRepository.findCompany(id);
  }

  update(id: string, updateCompanyDto: UpdateCompanyDto): Promise<Company> {
    return this.companyRepository.updateCompany(id, updateCompanyDto);
  }

  remove(id: string): Promise<void> {
    return this.companyRepository.deleteCompany(id);
  }
}
