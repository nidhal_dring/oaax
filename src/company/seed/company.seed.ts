import { Connection, Repository } from 'typeorm';
import { Company } from '../entities/company.entity';
import companies from './companies.json';

export class CompanySeed {
  private readonly connection: Connection;
  private readonly companyRepository: Repository<Company>;

  constructor(connection: Connection) {
    this.connection = connection;
    this.companyRepository = connection.getRepository(Company);
  }

  async run(): Promise<void> {
    await this.deleteAllCompanies();
    await this.createCompanies();
  }

  async deleteAllCompanies(): Promise<void> {
    await this.companyRepository.delete({});
  }

  async createCompanies(): Promise<void> {
    await Promise.all(
      companies.map((company) => {
        return this.companyRepository.save(company);
      }),
    );
  }
}
