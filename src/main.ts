import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app/app.module';
import figlet from 'figlet';
import { configService } from './config/config.service';
import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerModule,
} from '@nestjs/swagger';
import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common';
import { QueryExceptionFilter } from './commun/filters/query-exception.filter';
import { HttpExceptionFilter } from './commun/filters/http-exception.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  if (!configService.isProduction()) {
    // ╔═╗╦ ╦╔═╗╔═╗╔═╗╔═╗╦═╗
    // ╚═╗║║║╠═╣║ ╦║ ╦║╣ ╠╦╝
    // ╚═╝╚╩╝╩ ╩╚═╝╚═╝╚═╝╩╚═
    const customOptions: SwaggerCustomOptions = {
      swaggerOptions: {
        persistAuthorization: true,
      },
      customSiteTitle: 'OAAX API',
    };

    const options = new DocumentBuilder()
      .setTitle('OAAX API')
      .setDescription('APPLE JUICE PRODUCTION')
      .setVersion('1.0')
      .addBearerAuth()
      .build();

    const document = SwaggerModule.createDocument(app, options, {
      deepScanRoutes: true,
    });

    SwaggerModule.setup('api', app, document, customOptions);
  }

  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalFilters(new QueryExceptionFilter());
  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));
  app.enableCors();

  const port = process.env.PORT || 3000;
  await app.listen(port, () => {
    figlet('2021 - OAAX API', (_, data) => {
      console.log('\x1b[1m\x1b[32m%s\x1b[0m', data);
      figlet('Powered By APPLE JUICE DEV ', { font: 'Small' }, (a, res) =>
        console.log('\x1b[35m%s\x1b[0m', res),
      );
    });
    console.log(`Listening at http://localhost :${port}/`);
  });
}

bootstrap();
