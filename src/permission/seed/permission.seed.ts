import { Connection, Repository } from 'typeorm';
import { Permission } from '../entities/permission.entity';
import permissions from './permissions.json';

export class PermissionSeed {
  private readonly connection: Connection;
  private readonly permissionRepository: Repository<Permission>;

  constructor(connection: Connection) {
    this.connection = connection;
    this.permissionRepository = connection.getRepository(Permission);
  }

  async run(): Promise<void> {
    await this.deleteAllPermissions();
    await this.createPermissions();
  }

  async deleteAllPermissions(): Promise<void> {
    await this.permissionRepository.delete({});
  }

  async createPermissions(): Promise<void> {
    await Promise.all(
      permissions.map((permission) => {
        this.permissionRepository.save(permission);
      }),
    );
  }
}
