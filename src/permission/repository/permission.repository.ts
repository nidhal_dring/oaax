import { EntityRepository, Repository } from 'typeorm';
import { Permission } from '../entities/permission.entity';
import { CreatePermissionDto } from '../dto/create-permission.dto';
import { UpdatePermissionDto } from '../dto/update-permission.dto';
import { NotFoundException } from '@nestjs/common';
import { Errors } from '../../commun/enums/Errors.enum';

@EntityRepository(Permission)
export class PermissionRepository extends Repository<Permission> {
  async createPermission(
    createPermissionDto: CreatePermissionDto,
  ): Promise<Permission> {
    const permission = new Permission();
    Object.assign(permission, createPermissionDto);
    return await this.save(permission);
  }

  async findAllPermission(): Promise<Map<string, []>> {
    const permissions = await this.find();
    const categories = new Map();
    for (const permission of permissions) {
      if (!categories.has(permission.category)) {
        categories.set(permission.category, [
          { id: permission.id, name: permission.name },
        ]);
      } else {
        const cat = categories.get(permission.category);
        cat.push({ id: permission.id, name: permission.name });
      }
    }
    return categories;
  }

  async findPermission(id: string): Promise<Permission> {
    const permission = await this.findOne(id);
    if (!permission) {
      throw new NotFoundException(Errors.PERMISSION_NOT_FOUND);
    }
    return permission;
  }

  async updatePermission(
    id: string,
    updatePermissionDto: UpdatePermissionDto,
  ): Promise<Permission> {
    const permission = await this.findPermission(id);
    Object.assign(permission, updatePermissionDto);
    return this.save(permission);
  }

  async deletePermission(id: string): Promise<void> {
    const result = await this.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(Errors.PERMISSION_NOT_FOUND);
    }
  }
}
