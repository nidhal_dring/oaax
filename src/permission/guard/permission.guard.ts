import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Permission } from '../entities/permission.entity';
import { PERMISSIONS_KEY } from '../decorator/permission.decorator';
import { Observable } from 'rxjs';

@Injectable()
export class PermissionGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const permissions = this.reflector.getAllAndOverride(PERMISSIONS_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    const { user } = context.switchToHttp().getRequest();
    const userPermissions = user.role?.permissions?.map(
      (perm: Permission) => perm.name,
    );

    if (!userPermissions) {
      return false;
    }
    return permissions.some((perm) => userPermissions.indexOf(perm) !== -1);
  }
}
