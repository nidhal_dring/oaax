import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { PermissionService } from './permission.service';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';
import { ApiBearerAuth, ApiHeader, ApiTags } from '@nestjs/swagger';
import { ResponsePermissionDto } from './dto/response-permission.dto';
import { AuthGuard } from '@nestjs/passport';
import { PermissionGuard } from './guard/permission.guard';
import { PermissionRequired } from './decorator/permission.decorator';
import { PermissionEnum } from './enum/permission.enum';
import { Languages } from 'src/commun/enums/languages.enum';

@Controller('api/permission')
@ApiBearerAuth()
// @UseGuards(AuthGuard('jwt'), PermissionGuard)
@UseGuards(AuthGuard('jwt'))
@ApiTags('Permission')
@ApiHeader({ name: 'Accept-Language', enum: Languages })
export class PermissionController {
  constructor(private readonly permissionService: PermissionService) {}

  // @PermissionRequired(
  //   PermissionEnum.CREATE_PERMISSION,
  //   PermissionEnum.MANAGE_PERMISSION,
  // )
  @Post()
  create(
    @Body() createPermissionDto: CreatePermissionDto,
  ): Promise<ResponsePermissionDto> {
    return this.permissionService.createPermission(createPermissionDto);
  }

  // @PermissionRequired(
  //   PermissionEnum.VIEW_PERMISSION,
  //   PermissionEnum.MANAGE_PERMISSION,
  // )
  @Get()
  findAll(): Promise<Map<string, []>> {
    return this.permissionService.findAll();
  }

  // @PermissionRequired(
  //   PermissionEnum.VIEW_PERMISSION,
  //   PermissionEnum.MANAGE_PERMISSION,
  // )
  @Get(':id')
  findOne(@Param('id') id: string): Promise<ResponsePermissionDto> {
    return this.permissionService.findOne(id);
  }

  // @PermissionRequired(
  //   PermissionEnum.UPDATE_PERMISSION,
  //   PermissionEnum.MANAGE_PERMISSION,
  // )
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updatePermissionDto: UpdatePermissionDto,
  ): Promise<ResponsePermissionDto> {
    return this.permissionService.update(id, updatePermissionDto);
  }

  // @PermissionRequired(
  //   PermissionEnum.DELETE_PERMISSION,
  //   PermissionEnum.MANAGE_PERMISSION,
  // )
  @Delete(':id')
  remove(@Param('id') id: string): Promise<void> {
    return this.permissionService.remove(id);
  }
}
