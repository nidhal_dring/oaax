import { Injectable } from '@nestjs/common';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';
import { PermissionRepository } from './repository/permission.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Permission } from './entities/permission.entity';

@Injectable()
export class PermissionService {
  constructor(
    @InjectRepository(PermissionRepository)
    private readonly permissionRepository: PermissionRepository,
  ) {}

  createPermission(
    createPermissionDto: CreatePermissionDto,
  ): Promise<Permission> {
    return this.permissionRepository.createPermission(createPermissionDto);
  }

  findAll(): Promise<Map<string, []>> {
    return this.permissionRepository.findAllPermission();
  }

  findOne(id: string): Promise<Permission> {
    return this.permissionRepository.findPermission(id);
  }

  update(
    id: string,
    updatePermissionDto: UpdatePermissionDto,
  ): Promise<Permission> {
    return this.permissionRepository.updatePermission(id, updatePermissionDto);
  }

  remove(id: string): Promise<void> {
    return this.permissionRepository.deletePermission(id);
  }
}
