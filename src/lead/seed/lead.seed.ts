import { Connection, Repository } from 'typeorm';
import { Lead } from '../entities/lead.entity';
import leads from './leads.json';

export class LeadSeed {
  private readonly connection: Connection;
  private readonly leadRepository: Repository<Lead>;

  constructor(connection: Connection) {
    this.connection = connection;
    this.leadRepository = connection.getRepository(Lead);
  }

  async run(): Promise<void> {
    await this.deleteAllLeads();
    await this.createLeads();
  }

  async deleteAllLeads(): Promise<void> {
    await this.leadRepository.delete({});
    console.log('All leads deleted');
  }

  async createLeads(): Promise<void> {
    await Promise.all(
      leads.map((lead) => {
        return this.leadRepository.save((lead as any) as Lead);
      }),
    );
  }
}
