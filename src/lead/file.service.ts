import { Injectable } from '@nestjs/common';
import del from 'del';
import { InjectRepository } from '@nestjs/typeorm';
import { File } from './entities/file.entity';
import { Repository } from 'typeorm';
import { CreateFileDto } from './dto/create-file.dto';
import { join } from 'path';

@Injectable()
export class FileService {
  constructor(
    @InjectRepository(File)
    private fileRepository: Repository<File>,
  ) {}

  async createFile(createFileDto: CreateFileDto): Promise<File> {
    return this.fileRepository.save(this.fileRepository.create(createFileDto));
  }

  async removeFile(id: string): Promise<void> {
    const { destination, filename } = await this.fileRepository.findOne(id, {
      select: ['destination', 'filename'],
    });

    await del(join(destination, filename));
    await this.fileRepository.delete(id);
    return;
  }
}
