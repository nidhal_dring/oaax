import { forwardRef, Module } from '@nestjs/common';
import { LeadService } from './lead.service';
import { LeadController } from './lead.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LeadRepository } from './repository/lead.repository';
import { LanguageModule } from '../language/language.module';
import { AppointmentModule } from 'src/appointment/appointment.module';
import { InsuranceModule } from '../insurance/insurance.module';
import { UserModule } from '../user/user.module';
import { PinnedLeadModule } from '../pinned-lead/pinned-lead.module';
import { FileService } from './file.service';
import { File } from './entities/file.entity';
import { MulterModule } from '@nestjs/platform-express';
import { multerConfig } from './config/multer.config';
import { PersonRepository } from './repository/person.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([LeadRepository, PersonRepository, File]),
    LanguageModule,
    AppointmentModule,
    InsuranceModule,
    UserModule,
    forwardRef(() => PinnedLeadModule),
    MulterModule.register(multerConfig),
  ],
  controllers: [LeadController],
  providers: [LeadService, FileService],
  exports: [LeadService],
})
export class LeadModule {}
