import { Readable } from 'stream';
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  Res,
  ParseArrayPipe,
  UseInterceptors,
  UploadedFile,
  ParseUUIDPipe,
} from '@nestjs/common';
import { Express, Response } from 'express';
import { LeadService } from './lead.service';
import { CreateLeadDto } from './dto/create-lead.dto';
import { UpdateLeadDto } from './dto/update-lead.dto';
import { DownLoadLeadsDto } from './dto/download-leads.dto';
import { Lead } from './entities/lead.entity';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiHeader,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import { LeadFilterDto } from './dto/get-lead-filter.dto';
import { Pagination } from 'nestjs-typeorm-paginate';
import { CurrentUser } from '../user/decorator/user.decorator';
import { User } from '../user/entities/user.entity';
import { Languages } from 'src/commun/enums/languages.enum';
import { FileInterceptor } from '@nestjs/platform-express';
import { File } from './entities/file.entity';
import { FileService } from './file.service';
import { UploadFileDto } from './dto/upload-file.dto';
import { createReadStream } from 'fs';
import { join } from 'path';
import { AddLeadDto } from './dto/add-lead.dto';

@Controller('api/lead')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@ApiTags('Lead')
@ApiHeader({ name: 'Accept-Language', enum: Languages })
export class LeadController {
  constructor(
    private readonly leadService: LeadService,
    private readonly fileService: FileService,
  ) {}

  // @UseGuards(AuthGuard('jwt'), PermissionGuard)
  // @PermissionRequired(PermissionEnum.CREATE_LEAD, PermissionEnum.MANAGE_LEAD)
  @Post()
  create(@CurrentUser() user: User, @Body() addLeadDto: AddLeadDto): any {
    return this.leadService.createLead(addLeadDto, user);
  }

  // @UseGuards(AuthGuard('jwt'), PermissionGuard)
  // @PermissionRequired(PermissionEnum.CREATE_LEAD, PermissionEnum.MANAGE_LEAD)
  @Post('/:id/files/upload')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('file'))
  createFile(
    @Param('id', new ParseUUIDPipe()) leadId: string,
    @CurrentUser() user: User,
    @Body() uploadFileDto: UploadFileDto,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<File> {
    return this.fileService.createFile({
      ...file,
      lead: leadId,
      createdBy: user.id,
    });
  }

  // @UseGuards(AuthGuard('jwt'))
  // @PermissionRequired(PermissionEnum.VIEW_LEAD, PermissionEnum.MANAGE_LEAD)
  @Get('files/:filename/download')
  downloadFile(
    @Param('filename') filename: string,
    @Res() res: Response,
  ): Buffer {
    const file = createReadStream(
      join(process.env.FILE_UPLOAD_DIRECTORY_PATH, filename),
    );
    res.attachment(filename);
    file.pipe(res);
    return;
  }

  //without guard for now
  @ApiQuery({ name: 'ids', required: false })
  @Get('export.xlsx')
  async extractOnCsvFormat(
    @Res() res: Response,
    @Query() queryString: DownLoadLeadsDto,
    @Query(
      'ids',
      new ParseArrayPipe({ items: Number, separator: ',', optional: true }),
    )
    ids: string[],
  ): Promise<void> {
    console.log({ queryString });
    console.log({ ids });

    const bufferData = await this.leadService.findAllOnCsvFormat({
      ...queryString,
      ids,
    });
    const stream = new Readable();
    stream.push(bufferData);
    stream.push(null);
    res.set({
      'Content-Type': 'text/csv',
      'Content-Length': bufferData.length,
    });

    stream.pipe(res);
  }

  // @UseGuards(AuthGuard('jwt'), PermissionGuard)
  // @PermissionRequired(PermissionEnum.VIEW_LEAD, PermissionEnum.MANAGE_LEAD)
  @Get()
  findAll(
    @CurrentUser() user: User,
    @Query() pagination: LeadFilterDto,
  ): Promise<Pagination<Lead>> {
    return this.leadService.findAll(user, pagination);
  }

  // @UseGuards(AuthGuard('jwt'), PermissionGuard)
  // @PermissionRequired(PermissionEnum.VIEW_LEAD, PermissionEnum.MANAGE_LEAD)
  @Get('pinned-leads')
  findPinnedLeads(@CurrentUser() user: User): Promise<Lead[]> {
    console.log('------->>', user);
    return this.leadService.findPinnedLead(user);
  }

  // @UseGuards(AuthGuard('jwt'), PermissionGuard)
  // @PermissionRequired(PermissionEnum.VIEW_LEAD, PermissionEnum.MANAGE_LEAD)
  @Get(':id')
  findOne(@Param('id') id: string): Promise<Lead> {
    return this.leadService.findOne(id);
  }

  // @UseGuards(AuthGuard('jwt'), PermissionGuard)
  // @PermissionRequired(PermissionEnum.UPDATE_LEAD, PermissionEnum.MANAGE_LEAD)
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateLeadDto: UpdateLeadDto,
  ): Promise<Lead> {
    return this.leadService.update(id, updateLeadDto);
  }

  // @UseGuards(AuthGuard('jwt'), PermissionGuard)
  // @PermissionRequired(PermissionEnum.DELETE_LEAD, PermissionEnum.MANAGE_LEAD)
  @Delete(':id')
  remove(@Param('id') id: string): Promise<void> {
    return this.leadService.remove(id);
  }

  // @UseGuards(AuthGuard('jwt'), PermissionGuard)
  // @PermissionRequired(PermissionEnum.CREATE_LEAD, PermissionEnum.MANAGE_LEAD)
  @Delete('/files/:id')
  deleteFile(@Param('id', new ParseUUIDPipe()) fileId: string): Promise<void> {
    return this.fileService.removeFile(fileId);
  }
}
