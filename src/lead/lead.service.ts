import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { UpdateLeadDto } from './dto/update-lead.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { LeadRepository } from './repository/lead.repository';
import { Lead } from './entities/lead.entity';
import { LanguageService } from '../language/language.service';
import { LeadFilterDto } from './dto/get-lead-filter.dto';
import { AppointmentService } from 'src/appointment/appointment.service';
import convertToCsv from '../commun/utils/convertEntityDataToCsvFormat';
import { DownLoadLeadsDto } from './dto/download-leads.dto';
import { User } from '../user/entities/user.entity';
import { InsuranceService } from '../insurance/insurance.service';
import { UserService } from '../user/user.service';
import { PinnedLeadService } from '../pinned-lead/pinned-lead.service';
import { AddLeadDto } from './dto/add-lead.dto';
import { PersonRepository } from './repository/person.repository';
import { PersonDto } from './dto/person.dto';
import { Person } from './entities/person.entity';

@Injectable()
export class LeadService {
  constructor(
    @InjectRepository(LeadRepository)
    private readonly leadRepository: LeadRepository,
    @InjectRepository(PersonRepository)
    private readonly personRepository: PersonRepository,
    private readonly languageService: LanguageService,
    private readonly appointmentService: AppointmentService,
    private readonly insuranceService: InsuranceService,
    private readonly userService: UserService,
    @Inject(forwardRef(() => PinnedLeadService))
    private readonly pinnedLeadService: PinnedLeadService,
  ) {}

  async createPerson(person: PersonDto): Promise<Person> {
    let insurance;
    if (person.insurance) {
      insurance = await this.insuranceService.createInsurance(person.insurance);
    }
    return this.personRepository.createPerson(person, insurance);
  }

  async createLead(addLeadDto: AddLeadDto, user: User): Promise<Lead> {
    let languages;
    let appointment;
    let partnerPerson;

    if (addLeadDto.mainPerson.languages) {
      languages = await this.languageService.extractLanguages(
        addLeadDto.mainPerson.languages,
      );
    }
    if (addLeadDto.mainPerson.appointment) {
      appointment = await this.appointmentService.create(
        addLeadDto.mainPerson.appointment,
      );
    }

    const mainPerson = await this.createPerson(addLeadDto.mainPerson.person);
    if (addLeadDto.partner) {
      partnerPerson = await this.createPerson(addLeadDto.partner);
    }

    const lead = await this.leadRepository.createLead(
      mainPerson,
      languages,
      [appointment],
      user,
      partnerPerson,
    );

    if (addLeadDto.children) {
      for (const child of addLeadDto.children) {
        const childEntity = new Person();
        Object.assign(childEntity, child);
        if (child.insurance) {
          childEntity.insurance = await this.insuranceService.createInsurance(
            child.insurance,
          );
        }
        childEntity.mainPerson = lead;
        await this.personRepository.save(childEntity);
      }
    }

    return this.findOne(lead.id);
  }

  async findAll(user: User, options: LeadFilterDto): Promise<any> {
    const result = await this.leadRepository.findLeads(options);
    result.items.forEach(
      (item: any) =>
        (item.mainPerson.fullName =
          item.mainPerson.firstName + ' ' + item.mainPerson.lastName),
    );
    return result;
  }

  async findPinnedLead(user: User): Promise<any> {
    const leadIds = await this.pinnedLeadService.getLeadIdsByUserId(user.id);
    const result = await this.pinnedLeadService.findPinnedLead(leadIds);
    result.forEach(
      (item: any) => (item.fullName = item.firstName + ' ' + item.lastName),
    );
    return result;
  }

  findOne(id: string): Promise<Lead> {
    return this.leadRepository.findLead(id);
  }

  async getLeadsByIds(leadIds: string[]): Promise<Lead[]> {
    const leads = [];
    for (const leadId of leadIds) {
      const lead = await this.findOne(leadId);
      leads.push(lead);
    }
    return leads;
  }

  async update(id: string, updateLeadDto: UpdateLeadDto): Promise<Lead> {
    if (updateLeadDto.languages) {
      const languages = await this.languageService.extractLanguages(
        updateLeadDto.languages,
      );
      await this.languageService.deleteLeadLanguages(id);
      return this.leadRepository.updateLead(id, updateLeadDto, languages);
    }
    return this.leadRepository.updateLead(id, updateLeadDto);
  }

  remove(id: string): Promise<void> {
    return this.leadRepository.deleteLead(id);
  }

  async findAllOnCsvFormat(queryString: DownLoadLeadsDto): Promise<Buffer> {
    let leads;

    //* we do this because ts doesn't support overload methods
    if (queryString.ids) {
      leads = await this.leadRepository.findLeadsByIds(queryString.ids);
    } else leads = (await this.leadRepository.findLeads(queryString)).items;

    return convertToCsv<Lead>(leads[0], leads, [
      'languages',
      'appointments',
      'insurance',
      'callAgent',
      'files',
      'createdAt',
      'updatedAt',
    ]);
  }
}
