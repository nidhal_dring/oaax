import { Type } from 'class-transformer';
import { isArray, IsOptional } from 'class-validator';
import { addMainPersonDto } from './add-main-person.dto';
import { PersonDto } from './person.dto';

export class AddLeadDto {
  @Type(() => addMainPersonDto)
  mainPerson: addMainPersonDto;

  @IsOptional()
  @Type(() => PersonDto)
  partner: PersonDto;

  @IsOptional()
  @Type(() => PersonDto)
  children: PersonDto[];
}
