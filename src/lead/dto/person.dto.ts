import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsDateString,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import { CreateInsuranceDto } from 'src/insurance/dto/create-insurance.dto';
import { Gender } from '../enum/gender.enum';

export class PersonDto {
  @ApiProperty({ default: new Date() })
  @IsDateString()
  @IsOptional()
  dateOfBirth: Date;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  source: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  firstName: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  lastName: string;

  @IsEmail()
  @IsOptional()
  email: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  street: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  plz: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  place: string;

  @IsString()
  @IsNotEmpty()
  phoneNumber: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  phoneNate: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  comment: string;

  @IsEnum(Gender)
  @IsOptional()
  gender: Gender;

  @IsOptional()
  @Type(() => CreateInsuranceDto)
  insurance: CreateInsuranceDto;
}
