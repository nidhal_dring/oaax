import { ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsDateString,
  IsEnum,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
  Matches,
} from 'class-validator';
import { Gender } from '../enum/gender.enum';
import { Transform } from 'class-transformer';
export class LeadFilterDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  search: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  fullName: string;

  @ApiPropertyOptional({
    example: 'firstName:asc',
  })
  @IsString()
  @IsOptional()
  @Matches(/:(ASC|DESC)$/im)
  order: string;

  @ApiPropertyOptional({ minimum: 0, type: Number, default: 10 })
  @IsOptional()
  @IsPositive()
  @IsNumber()
  @Transform(({ value }) => parseInt(value, 10))
  limit: number;

  @ApiPropertyOptional({ minimum: 1, type: Number, default: 1 })
  @IsOptional()
  @IsNumber()
  @IsPositive()
  @Transform(({ value }) => parseInt(value, 10))
  page: number;

  @ApiPropertyOptional()
  @IsEnum(Gender)
  @IsOptional()
  gender: Gender;

  @ApiPropertyOptional()
  @IsDateString()
  @IsOptional()
  dateOfBirth: Date;

  @ApiPropertyOptional()
  @IsDateString()
  @IsOptional()
  createdAt: Date;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  source: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  firstName: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  lastName: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  email: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  street: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  plz: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  place: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  phoneNumber: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  phoneNate: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  comment: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  status: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  category: string;

  @ApiPropertyOptional()
  @IsString({ each: true })
  @IsOptional()
  callAgents: string[];

  @ApiPropertyOptional()
  @IsString({ each: true })
  @IsOptional()
  languages: string[];
}
