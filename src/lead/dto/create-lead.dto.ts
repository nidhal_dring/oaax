import {
  IsDateString,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Gender } from '../enum/gender.enum';
import { CreateAppointmentDto } from 'src/appointment/dto/create-appointment.dto';
import { Type } from 'class-transformer';
import { CreateInsuranceDto } from '../../insurance/dto/create-insurance.dto';

export class CreateLeadDto {
  @ApiProperty({ default: new Date() })
  @IsDateString()
  @IsOptional()
  dateOfBirth: Date;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  source: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  firstName: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  lastName: string;

  @IsEmail()
  @IsOptional()
  email: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  street: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  plz: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  place: string;

  @IsString()
  @IsNotEmpty()
  phoneNumber: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  phoneNate: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  comment: string;

  @IsEnum(Gender)
  @IsOptional()
  gender: Gender;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  status: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  category: string;

  @IsString({ each: true })
  @IsOptional()
  languages: string[];

  @Type(() => CreateAppointmentDto)
  appointment: CreateAppointmentDto;

  @Type(() => CreateInsuranceDto)
  insurance: CreateInsuranceDto;
}
