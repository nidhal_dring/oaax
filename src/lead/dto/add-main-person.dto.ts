import { Type } from 'class-transformer';
import { IsOptional, IsString, IsUUID } from 'class-validator';
import { CreateAppointmentDto } from 'src/appointment/dto/create-appointment.dto';
import { PersonDto } from './person.dto';

export class addMainPersonDto {
  @Type(() => PersonDto)
  person: PersonDto;

  @IsUUID('all', { each: true })
  @IsOptional()
  languages: string[];

  @IsOptional()
  @Type(() => CreateAppointmentDto)
  appointment: CreateAppointmentDto;
}
