import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import { Gender } from '../enum/gender.enum';

export class UpdateLeadDto {
  @IsOptional()
  @ApiProperty({ default: new Date() })
  @IsDateString()
  dateOfBirth: Date;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  source: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  lastName: string;

  @IsOptional()
  @IsEmail()
  email: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  street: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  plz: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  place: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  phoneNumber: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  phoneNate: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  comment: string;

  @IsOptional()
  @IsEnum(Gender)
  gender: Gender;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  status: string;

  @IsString()
  @IsNotEmpty()
  @IsOptional()
  category: string;

  @IsOptional()
  @IsString({ each: true })
  languages: string[];
}
