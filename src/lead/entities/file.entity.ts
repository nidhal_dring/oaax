import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';

import { User } from '../../user/entities/user.entity';
import { Lead } from './lead.entity';

@Entity()
export class File {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  mimetype: string;

  @Column()
  filename: string;

  @Column()
  originalname: string;

  @Column()
  extension: string;

  @Column()
  size: number;

  @Column({ select: false })
  destination: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(() => User)
  createdBy: User | string;

  @ManyToOne(() => Lead, (lead) => lead.files)
  lead: Lead | string;
}
