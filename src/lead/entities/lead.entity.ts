import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { Gender } from '../enum/gender.enum';
import { Language } from '../../language/entities/language.entity';
import { Appointment } from '../../appointment/entities/appointment.entity';
import { Insurance } from '../../insurance/entity/insurance.entity';
import { User } from '../../user/entities/user.entity';
import { PinnedLead } from '../../pinned-lead/entity/pinned-lead.entity';
import { File } from './file.entity';
import { Person } from './person.entity';

@Entity()
export class Lead {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToOne(() => Person)
  @JoinColumn()
  mainPerson: Person;

  @OneToOne(() => Person)
  @JoinColumn()
  partner: Person;

  @OneToMany(() => Person, (person) => person.mainPerson)
  children: Person[];

  @ManyToOne(() => User, (user) => user.leads)
  callAgent: User;

  @ManyToMany(() => Language, (language) => language.leads)
  @JoinTable()
  languages: Language[];

  @OneToMany(() => Appointment, (appointment) => appointment.lead, {
    onDelete: 'CASCADE',
  })
  appointments: Appointment[];

  @OneToMany(() => PinnedLead, (pinnedLead) => pinnedLead.lead)
  pinnedLeads: PinnedLead[];

  @OneToMany(() => File, (file) => file.lead)
  files: File[];

  @CreateDateColumn()
  createdAt: string;

  @Exclude()
  @UpdateDateColumn()
  updatedAt: string;
}
