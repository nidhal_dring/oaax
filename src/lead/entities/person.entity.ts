import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { Language } from '../../language/entities/language.entity';
import { Appointment } from '../../appointment/entities/appointment.entity';
import { Insurance } from '../../insurance/entity/insurance.entity';
import { User } from '../../user/entities/user.entity';
import { PinnedLead } from '../../pinned-lead/entity/pinned-lead.entity';
import { File } from './file.entity';
import { Gender } from '../enum/gender.enum';
import { Lead } from './lead.entity';

@Entity()
export class Person {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  phoneNumber: string;

  @Column('enum', { enum: Gender, nullable: true })
  gender: Gender;

  @Column({ nullable: true })
  dateOfBirth: Date;

  @Column({ nullable: true })
  source: string;

  @Column({ nullable: true })
  firstName: string;

  @Column({ nullable: true })
  lastName: string;

  @Column({ nullable: true })
  email: string;

  @Column({ nullable: true })
  street: string;

  @Column({ nullable: true })
  plz: string;

  @Column({ nullable: true })
  place: string;

  @Column({ nullable: true })
  phoneNate: string;

  @Column({ nullable: true })
  comment: string;

  @ManyToOne(() => Insurance, (insurance) => insurance.persons)
  insurance: Insurance;

  @ManyToOne(() => Lead, (mainPerson) => mainPerson.children)
  mainPerson: Lead;
}
