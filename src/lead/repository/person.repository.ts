import { Insurance } from 'src/insurance/entity/insurance.entity';
import { EntityRepository, Repository } from 'typeorm';
import { PersonDto } from '../dto/person.dto';
import { Person } from '../entities/person.entity';

@EntityRepository(Person)
export class PersonRepository extends Repository<Person> {
  async createPerson(personDto: PersonDto, insurance: Insurance) {
    const person = new Person();
    Object.assign(person, personDto);
    if (insurance) {
      person.insurance = insurance;
    }
    person.insurance = insurance;
    await this.save(person);
    return this.findOne(person.id, { relations: ['insurance'] });
  }
}
