import { EntityRepository, Repository } from 'typeorm';
import { Lead } from '../entities/lead.entity';
import { CreateLeadDto } from '../dto/create-lead.dto';
import { Language } from '../../language/entities/language.entity';
import { NotFoundException } from '@nestjs/common';
import { Errors } from '../../commun/enums/Errors.enum';
import { UpdateLeadDto } from '../dto/update-lead.dto';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';
import { LeadFilterDto } from '../dto/get-lead-filter.dto';
import { Appointment } from 'src/appointment/entities/appointment.entity';
import { Insurance } from '../../insurance/entity/insurance.entity';
import { User } from '../../user/entities/user.entity';
import { PersonDto } from '../dto/person.dto';
import { Person } from '../entities/person.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { PersonRepository } from './person.repository';

@EntityRepository(Lead)
export class LeadRepository extends Repository<Lead> {
  constructor(
    @InjectRepository(PersonRepository)
    private readonly personRepo: PersonRepository,
  ) {
    super();
  }

  async createLead(
    person: Person,
    languages: Language[],
    appointments: Appointment[],
    user: User,
    partnerPerson?: Person,
    // children?: Person[],
  ): Promise<Lead> {
    const lead = new Lead();
    if (languages) {
      lead.languages = languages;
    }
    if (appointments) {
      lead.appointments = appointments;
    }

    lead.callAgent = user;
    lead.mainPerson = person;
    lead.partner = partnerPerson;
    // lead.children = children;

    await this.save(lead);
    return this.findOne(lead.id, {
      relations: [
        'languages',
        'appointments',
        'mainPerson',
        'appointments.language',
        'callAgent',
      ],
    });
  }

  async findLeads(options: LeadFilterDto): Promise<Pagination<Lead>> {
    const result = await this.createQueryBuilder('lead')
      .leftJoinAndSelect('lead.mainPerson', 'mainPerson')
      .leftJoinAndSelect('lead.languages', 'languages')
      .leftJoinAndSelect('lead.appointments', 'appointments')
      .leftJoinAndSelect('appointments.language', 'language')
      .leftJoinAndSelect('mainPerson.insurance', 'insurance')
      .leftJoinAndSelect('insurance.healthInsurance', 'healthInsurance')
      .leftJoinAndSelect('insurance.contractDuration', 'contractDuration')
      .leftJoinAndSelect('insurance.hospitality', 'hospitality')
      .leftJoinAndSelect('lead.callAgent', 'callAgent')
      .leftJoinAndSelect('lead.files', 'files')
      .orderBy('lead.createdAt', 'DESC');

    if (options.search) {
      result.andWhere(
        `(
          CONCAT(LOWER(mainPerson.firstName),' ',LOWER(mainPerson.lastName)) like :search OR
          CONCAT(LOWER(mainPerson.lastName),' ',LOWER(mainPerson.firstName)) like :search OR
          LOWER(mainPerson.phoneNumber) like :search OR
          LOWER(mainPerson.source) like :search OR
          LOWER(mainPerson.email) like :search OR
          LOWER(mainPerson.street) like :search OR
          LOWER(mainPerson.plz) like :search OR
          LOWER(mainPerson.place) like :search OR
          LOWER(mainPerson.phoneNate) like :search OR
          LOWER(mainPerson.comment) like :search OR
          LOWER(appointments.status) like :search OR
          LOWER(appointments.category) like :search
            )`,
        { search: `%${options.search.toLowerCase()}%` },
      );
    }

    if (options.fullName) {
      result.andWhere(
        `(
          CONCAT(LOWER(mainPerson.firstName),' ',LOWER(mainPerson.lastName)) like :fullName OR
          CONCAT(LOWER(mainPerson.lastName),' ',LOWER(mainPerson.firstName)) like :fullName
          )`,
        { fullName: `%${options.fullName.toLowerCase()}%` },
      );
    }

    if (options.order) {
      const [fieldName, order] = options.order.split(':');
      const orderDirection = order.toUpperCase() as 'ASC' | 'DESC';
      if (fieldName === 'fullName') {
        result.orderBy('mainPerson.firstName', orderDirection);
        result.addOrderBy('mainPerson.lastName', orderDirection);
      } else {
        result.orderBy(`mainPerson.${fieldName}`, orderDirection);
      }
    }

    if (options.gender) {
      result.andWhere('mainPerson.gender = :gender', {
        gender: `${options.gender}`,
      });
    }
    if (options.dateOfBirth) {
      result.andWhere('mainPerson.dateOfBirth = :dateOfBirth', {
        dateOfBirth: `${options.dateOfBirth}`,
      });
    }
    if (options.createdAt) {
      result.andWhere(
        "TO_CHAR(mainPerson.createdAt,'YYYY-MM-DD') = :createdAt",
        {
          createdAt: `${options.createdAt}`,
        },
      );
    }

    if (options.plz) {
      result.andWhere('LOWER(mainPerson.plz) LIKE :plz', {
        plz: `%${options.plz.toLowerCase()}%`,
      });
    }
    if (options.email) {
      result.andWhere('LOWER(mainPerson.email) LIKE :email', {
        email: `%${options.email.toLowerCase()}%`,
      });
    }
    if (options.source) {
      result.andWhere('LOWER(mainPerson.source) LIKE :source', {
        source: `%${options.source.toLowerCase()}%`,
      });
    }
    if (options.firstName) {
      result.andWhere('LOWER(mainPerson.firstName) LIKE :firstName', {
        firstName: `%${options.firstName.toLowerCase()}%`,
      });
    }
    if (options.lastName) {
      result.andWhere('LOWER(mainPerson.lastName) LIKE :lastName', {
        lastName: `%${options.lastName.toLowerCase()}%`,
      });
    }
    if (options.street) {
      result.andWhere('LOWER(mainPerson.street) LIKE :street', {
        street: `%${options.street.toLowerCase()}%`,
      });
    }
    if (options.phoneNumber) {
      result.andWhere('LOWER(mainPerson.phoneNumber) LIKE :phoneNumber', {
        phoneNumber: `%${options.phoneNumber.toLowerCase()}%`,
      });
    }
    if (options.place) {
      result.andWhere('LOWER(mainPerson.place) LIKE :place', {
        place: `%${options.place.toLowerCase()}%`,
      });
    }
    if (options.phoneNate) {
      result.andWhere('LOWER(mainPerson.phoneNate) LIKE :phoneNate', {
        phoneNate: `%${options.phoneNate.toLowerCase()}%`,
      });
    }
    if (options.comment) {
      result.andWhere('LOWER(mainPerson.comment) LIKE :comment', {
        comment: `%${options.comment.toLowerCase()}%`,
      });
    }
    if (options.callAgents) {
      const callAgents = Array.isArray(options.callAgents)
        ? options.callAgents
        : [options.callAgents];
      result.andWhere('callAgent.id IN (:...callAgents)', {
        callAgents,
      });
    }
    if (options.languages) {
      const languages = Array.isArray(options.languages)
        ? options.languages
        : [options.languages];
      result.andWhere('languages.id IN (:...languages)', {
        languages,
      });
    }
    return paginate<Lead>(result, { limit: options.limit, page: options.page });
  }

  async findLeadsByIds(ids: string[]): Promise<Lead[]> {
    return this.createQueryBuilder('lead')
      .where('lead.id IN (:...ids)', { ids })
      .orderBy('lead.createdAt', 'DESC')
      .getMany();
  }

  async findLead(id: string): Promise<Lead> {
    const lead = await this.findOne(id, {
      relations: [
        'languages',
        'appointments',
        'appointments.language',
        'callAgent',
        'mainPerson',
        'mainPerson.insurance',
        'partner',
        'partner.insurance',
        'children',
        'files',
      ],
    });
    if (!lead) {
      throw new NotFoundException(Errors.LEAD_NOT_FOUND);
    }
    return lead;
  }

  async deleteLead(id: string): Promise<void> {
    const result = await this.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(Errors.LEAD_NOT_FOUND);
    }
  }

  async updateLead(
    id: string,
    updateLeadDto: UpdateLeadDto,
    languages?: Language[],
  ): Promise<Lead> {
    const lead = await this.findLead(id);
    Object.assign(lead, updateLeadDto);
    if (!languages) {
      return this.save(lead);
    }
    lead.languages = languages;
    return this.save(lead);
  }
}
