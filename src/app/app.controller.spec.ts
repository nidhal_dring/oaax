import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('dummy test', () => {
  let appService: AppService;
  let appController: AppController;

  beforeEach(() => {
    appService = new AppService();
    appController = new AppController(appService);
  });

  describe('Get /', () => {
    it('should salute the world !', async () => {
      const result = 'hello world !';
      expect(await appController.get()).toBe(result);
    });
  });
});
