import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from '../user/user.module';
import { configService } from '../config/config.service';
import { CompanyModule } from '../company/company.module';
import { RoleModule } from '../role/role.module';
import { PermissionModule } from '../permission/permission.module';
import { AuthenticationModule } from '../authentication/authentication.module';
import { EmailModule } from 'src/email/email.module';
import { LanguageModule } from '../language/language.module';
import { InvitedUserModule } from 'src/invited-users/invited-user.module';
import { LeadModule } from '../lead/lead.module';
import { AppointmentModule } from '../appointment/appointment.module';
import { InsuranceModule } from '../insurance/insurance.module';
import { PinnedLeadModule } from '../pinned-lead/pinned-lead.module';
import { UserPreferenceModule } from '../user-preference/user-preference.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, envFilePath: '.env.local' }),
    TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
    UserModule,
    CompanyModule,
    RoleModule,
    PermissionModule,
    AuthenticationModule,
    EmailModule,
    LanguageModule,
    InvitedUserModule,
    LeadModule,
    AppointmentModule,
    InsuranceModule,
    PinnedLeadModule,
    UserPreferenceModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
