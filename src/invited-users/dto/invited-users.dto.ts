import { IsUUID } from 'class-validator';

export class InvitedUsersDto {
  @IsUUID('4', { each: true })
  ids: string[];
}
