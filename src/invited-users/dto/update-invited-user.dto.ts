import { IsEmail, IsOptional, IsUUID } from 'class-validator';

export class UpdateInvitedUserDto {
  @IsOptional()
  @IsEmail()
  email: string;

  @IsOptional()
  @IsUUID()
  role: string;

  @IsOptional()
  @IsUUID()
  company: string;
}
