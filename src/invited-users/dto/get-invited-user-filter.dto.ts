import { ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsDateString,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
  Matches,
} from 'class-validator';
import { Transform } from 'class-transformer';

export class GetInvitedUserFilterDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  search: string;

  @ApiPropertyOptional({
    example: 'email:asc',
  })
  @IsString()
  @IsOptional()
  @Matches(/:(ASC|DESC)$/im)
  order: string;

  @ApiPropertyOptional({ minimum: 0, type: Number, default: 10 })
  @IsOptional()
  @IsPositive()
  @IsNumber()
  @Transform(({ value }) => parseInt(value, 10))
  limit: number;

  @ApiPropertyOptional({ minimum: 1, type: Number, default: 1 })
  @IsOptional()
  @IsNumber()
  @IsPositive()
  @Transform(({ value }) => parseInt(value, 10))
  page: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  email: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  role: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  company: string;

  @ApiPropertyOptional()
  @IsDateString()
  @IsOptional()
  createdAt: Date;
}
