import { IsEmail } from 'class-validator';

export class CreateTokenDto {
  @IsEmail()
  email: string;
}
