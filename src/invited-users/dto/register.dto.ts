import {
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class RegisterDto {
  @IsEmail()
  @IsOptional()
  secondaryEmail: string;

  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsString()
  @IsNotEmpty()
  lastName: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  pseudonym: string;

  @IsString()
  @IsNotEmpty()
  password: string;

  @ApiProperty({ default: new Date() })
  @IsDateString()
  dateOfBirth: Date;

  // @IsBoolean()
  // @IsOptional()
  // isActive: boolean;

  @IsString()
  token: string;
}
