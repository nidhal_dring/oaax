import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmailModule } from 'src/email/email.module';
import { UserModule } from 'src/user/user.module';
import { InvitedUserController } from './invited-user.controller';
import { InvitedUserService } from './invited-user.service';
import { InvitedUserRepository } from './repository/invited-user.repository';
import { RoleModule } from '../role/role.module';
import { CompanyModule } from '../company/company.module';

@Module({
  controllers: [InvitedUserController],
  providers: [InvitedUserService],
  imports: [
    forwardRef(() => UserModule),
    TypeOrmModule.forFeature([InvitedUserRepository]),
    EmailModule,
    RoleModule,
    CompanyModule,
  ],
  exports: [InvitedUserService],
})
export class InvitedUserModule {}
