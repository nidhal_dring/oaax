import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Company } from '../../company/entities/company.entity';
import { Role } from '../../role/entities/role.entity';

@Entity()
export class InvitedUser {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  email: string;

  @ManyToOne(() => Company, (company) => company.invitedUsers)
  company: Company;

  @ManyToOne(() => Role, (role) => role.invitedUsers)
  role: Role;

  @CreateDateColumn()
  createdAt: string;
}
