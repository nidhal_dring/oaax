import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { InvitedUserService } from './invited-user.service';
import { InvitedUserDto } from '../user/dto/invited-user.dto';
import { InvitedUser } from './entities/invited-user.entity';
import { InvitedUsersDto } from './dto/invited-users.dto';
import { UpdateInvitedUserDto } from './dto/update-invited-user.dto';
import { GetInvitedUserFilterDto } from './dto/get-invited-user-filter.dto';
import { Pagination } from 'nestjs-typeorm-paginate';

@Controller('/api/invited-users')
@ApiTags('Registration')
export class InvitedUserController {
  constructor(private readonly invitedUserService: InvitedUserService) {}

  @Post()
  addInvitedUser(@Body() invitedUserDto: InvitedUserDto): Promise<InvitedUser> {
    return this.invitedUserService.addUser(invitedUserDto);
  }

  @Get()
  getAllInvitedUsers(
    @Query() options: GetInvitedUserFilterDto,
  ): Promise<Pagination<InvitedUser>> {
    return this.invitedUserService.getAllInvitedUsers(options);
  }

  @Delete('/:id')
  async deleteInvitedUserById(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<void> {
    await this.invitedUserService.deleteInvitedUserByIdOrThrow(id);
  }

  @Delete()
  async deleteInvitedUsersByIds(
    @Query() invitedUsersDto: InvitedUsersDto,
  ): Promise<void> {
    await this.invitedUserService.deleteInvitedUsersByIds(invitedUsersDto.ids);
  }

  @Put('/:id')
  updateInvitedUser(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateInvitedUserDto: UpdateInvitedUserDto,
  ): Promise<InvitedUser> {
    return this.invitedUserService.updateInvitedUser(id, updateInvitedUserDto);
  }
}
