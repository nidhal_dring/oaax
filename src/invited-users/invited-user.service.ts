import {
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { EmailService } from 'src/email/email.service';
import { UserService } from 'src/user/user.service';
import { InvitedUserRepository } from './repository/invited-user.repository';
import { InvitedUserDto } from '../user/dto/invited-user.dto';
import { InvitedUser } from './entities/invited-user.entity';
import { RoleService } from '../role/role.service';
import { CompanyService } from '../company/company.service';
import { Errors } from '../commun/enums/Errors.enum';
import { UpdateInvitedUserDto } from './dto/update-invited-user.dto';
import { GetInvitedUserFilterDto } from './dto/get-invited-user-filter.dto';
import { Pagination } from 'nestjs-typeorm-paginate';

@Injectable()
export class InvitedUserService {
  constructor(
    private readonly invitedUserRepository: InvitedUserRepository,
    private readonly emailService: EmailService,
    private readonly roleService: RoleService,
    private readonly companyService: CompanyService,
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
  ) {}

  async updateInvitedUser(
    id: string,
    updateInvitedUserDto: UpdateInvitedUserDto,
  ): Promise<InvitedUser> {
    const invitedUser = await this.getInvitedUserOrThrow(id);

    if (updateInvitedUserDto.role) {
      await this.roleService.findOne(updateInvitedUserDto.role);
    }
    if (updateInvitedUserDto.company) {
      await this.companyService.findOne(updateInvitedUserDto.company);
    }
    return this.invitedUserRepository.updateInvitedUser(
      invitedUser,
      updateInvitedUserDto,
    );
  }

  async deleteInvitedUsersByIds(ids: string[]): Promise<void> {
    await this.invitedUserRepository.deleteInvitedUsersByIds(ids);
  }

  async getInvitedUserOrThrow(id: string): Promise<InvitedUser> {
    const invitedUser = await this.invitedUserRepository.findOne(id, {
      relations: ['role', 'company'],
    });
    if (!invitedUser) {
      throw new NotFoundException(Errors.INVITED_USER_NOT_FOUND);
    }
    return invitedUser;
  }

  async getInvitedUserByEmailOrThrow(email: string): Promise<InvitedUser> {
    const invitedUser = await this.invitedUserRepository.findOne({ email });
    if (!invitedUser) {
      throw new NotFoundException(Errors.INVITED_USER_NOT_FOUND);
    }
    return invitedUser;
  }

  async deleteInvitedUserByIdOrThrow(id: string): Promise<void> {
    await this.invitedUserRepository.deleteInvitedUserByIdOrThrow(id);
  }

  async getAllInvitedUsers(
    options: GetInvitedUserFilterDto,
  ): Promise<Pagination<InvitedUser>> {
    return this.invitedUserRepository.getAllInvitedUsers(options);
  }

  async addInvitedUser(invitedUserDto: InvitedUserDto): Promise<InvitedUser> {
    const invitedUser = await this.invitedUserRepository.addInvitedUser(
      invitedUserDto,
    );
    await this.emailService.sendInvitationEmail(invitedUser.email);
    return invitedUser;
  }

  async addUser(invitedUserDto: InvitedUserDto): Promise<InvitedUser> {
    await this.companyService.findOne(invitedUserDto.company);
    await this.roleService.findOne(invitedUserDto.role);
    return this.addInvitedUser(invitedUserDto);
  }

  async deleteInvitedUser(email: string): Promise<void> {
    await this.invitedUserRepository.delete({ email });
  }
}
