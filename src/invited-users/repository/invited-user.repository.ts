import { EntityRepository, Repository } from 'typeorm';
import { InvitedUser } from '../entities/invited-user.entity';
import { InvitedUserDto } from '../../user/dto/invited-user.dto';
import { NotFoundException } from '@nestjs/common';
import { Errors } from '../../commun/enums/Errors.enum';
import { UpdateInvitedUserDto } from '../dto/update-invited-user.dto';
import { GetInvitedUserFilterDto } from '../dto/get-invited-user-filter.dto';
import { paginate, Pagination } from 'nestjs-typeorm-paginate';

@EntityRepository(InvitedUser)
export class InvitedUserRepository extends Repository<InvitedUser> {
  async getAllInvitedUsers(
    options: GetInvitedUserFilterDto,
  ): Promise<Pagination<InvitedUser>> {
    const result = await this.createQueryBuilder('invited_user')
      .leftJoinAndSelect('invited_user.role', 'role')
      .leftJoinAndSelect('invited_user.company', 'company')
      .orderBy('invited_user.createdAt', 'DESC');
    if (options.search) {
      result.andWhere(
        `(
          LOWER(invited_user.email) like :search OR
          LOWER(role.name) like :search OR
          LOWER(company.name) like :search
            )`,
        { search: `%${options.search.toLowerCase()}%` },
      );
    }
    if (options.email) {
      result.andWhere('LOWER(invited_user.email) LIKE :email', {
        email: `%${options.email.toLowerCase()}%`,
      });
    }

    if (options.role) {
      result.andWhere('LOWER(role.name) LIKE :role', {
        role: `%${options.role.toLowerCase()}%`,
      });
    }

    if (options.company) {
      result.andWhere('LOWER(company.name) LIKE :company', {
        company: `%${options.company.toLowerCase()}%`,
      });
    }

    if (options.createdAt) {
      result.andWhere(
        "TO_CHAR(invited_user.createdAt,'YYYY-MM-DD') = :createdAt",
        {
          createdAt: `${options.createdAt}`,
        },
      );
    }

    if (options.order) {
      const args = options.order.split(':');
      const orderDirection = args[1].toUpperCase() as 'ASC' | 'DESC';
      let fieldName = args[0];
      if (!fieldName.includes('.')) {
        fieldName = `invited_user.${fieldName}`;
      }
      result.orderBy(fieldName, orderDirection);
    }
    return paginate<InvitedUser>(result, {
      limit: options.limit,
      page: options.page,
    });
  }

  async addInvitedUser(invitedUserDto: InvitedUserDto): Promise<InvitedUser> {
    const invitedUser = new InvitedUser();
    Object.assign(invitedUser, invitedUserDto);
    await this.save(invitedUser);
    return this.findOne(invitedUser.id, { relations: ['role', 'company'] });
  }

  async deleteInvitedUserByIdOrThrow(id: string): Promise<void> {
    const result = await this.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(Errors.INVITED_USER_NOT_FOUND);
    }
  }

  async deleteInvitedUsersByIds(ids: string[]): Promise<void> {
    await this.delete(ids);
  }

  async updateInvitedUser(
    invitedUser: InvitedUser,
    updateInvitedUserDto: UpdateInvitedUserDto,
  ): Promise<InvitedUser> {
    Object.assign(invitedUser, updateInvitedUserDto);
    await this.save(invitedUser);
    return this.findOne(invitedUser.id, { relations: ['role', 'company'] });
  }
}
