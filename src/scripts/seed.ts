import { ConnectionOptions, createConnection } from 'typeorm';
import { configService } from '../config/config.service';
import { CompanySeed } from '../company/seed/company.seed';
import { PermissionSeed } from '../permission/seed/permission.seed';
import { RoleSeed } from '../role/seeds/role.seed';
import { UserSeed } from 'src/user/seed/user.seed';
import { LeadSeed } from 'src/lead/seed/lead.seed';
import { LanguageSeed } from 'src/language/seed/language.seed';
import { InsuranceSeed } from '../insurance/seed/insurance.seed';

async function run() {
  const opt = {
    ...configService.getTypeOrmConfig(),
    debug: true,
  };

  const connection = await createConnection(opt as ConnectionOptions);

  const langSeed = new LanguageSeed(connection);
  const companySeed = new CompanySeed(connection);
  const permissionSeed = new PermissionSeed(connection);
  const roleSeed = new RoleSeed(connection);
  const userSeed = new UserSeed(connection);
  const leadSeed = new LeadSeed(connection);
  const insuranceSeed = new InsuranceSeed(connection);

  // order does matter !
  // @todo: investigate what's the correct delete order ??
  await langSeed.run();
  await permissionSeed.run();
  await roleSeed.run();
  await companySeed.run();
  await userSeed.run();
  await leadSeed.run();
  await insuranceSeed.run();
}

run()
  .then(() => console.log('...wait for script to exit'))
  .catch((error) => console.error('seed error', error));
