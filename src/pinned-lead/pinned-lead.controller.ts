import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { PinnedLeadService } from './pinned-lead.service';
import { ApiBearerAuth, ApiHeader, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { PermissionGuard } from '../permission/guard/permission.guard';
import { PermissionRequired } from '../permission/decorator/permission.decorator';
import { PermissionEnum } from '../permission/enum/permission.enum';
import { CurrentUser } from '../user/decorator/user.decorator';
import { User } from '../user/entities/user.entity';
import { PinnedLead } from './entity/pinned-lead.entity';
import { ActionEnum } from './enum/action.enum';
import { PinnedLeadsDto } from './dto/pinned-leads.dto';
import { Languages } from 'src/commun/enums/languages.enum';

@Controller('api/pinned-leads')
@ApiBearerAuth()
// @UseGuards(AuthGuard('jwt'), PermissionGuard)
@UseGuards(AuthGuard('jwt'))
@ApiTags('Pinned Leads')
@ApiHeader({ name: 'Accept-Language', enum: Languages })
export class PinnedLeadController {
  constructor(private readonly pinnedLeadService: PinnedLeadService) {}

  // @PermissionRequired(PermissionEnum.CREATE_LEAD, PermissionEnum.MANAGE_LEAD)
  @Post('/:idLead')
  create(
    @CurrentUser() user: User,
    @Param('idLead', ParseUUIDPipe) idLead: string,
  ): Promise<PinnedLead> {
    return this.pinnedLeadService.createPinnedLead(user, idLead);
  }

  // @PermissionRequired(PermissionEnum.DELETE_LEAD, PermissionEnum.MANAGE_LEAD)
  @Delete('/:idLead')
  delete(
    @CurrentUser() user: User,
    @Param('idLead', ParseUUIDPipe) idLead: string,
  ): Promise<void> {
    return this.pinnedLeadService.deletePinnedLead(user, idLead);
  }

  // @PermissionRequired(PermissionEnum.DELETE_LEAD, PermissionEnum.MANAGE_LEAD)
  // @Delete()
  // deletePinnedLeads(
  //   @CurrentUser() user: User,
  //   @Body() deletePinnedLeadsDto: DeletePinnedLeadsDto,
  // ): Promise<void> {
  //   return this.pinnedLeadService.deletePinnedLeads(user, deletePinnedLeadsDto);
  // }

  // @PermissionRequired(PermissionEnum.CREATE_LEAD, PermissionEnum.MANAGE_LEAD)
  // @Post()
  // createPinnedLeads(
  //   @CurrentUser() user: User,
  //   @Body() addPinnedLeadsDto: PinnedLeadsDto,
  // ): Promise<void> {
  //   return this.pinnedLeadService.createPinnedLeads(user, addPinnedLeadsDto);
  // }

  // @PermissionRequired(PermissionEnum.CREATE_LEAD, PermissionEnum.MANAGE_LEAD)
  @Put()
  ModifyPinnedLeads(
    @CurrentUser() user: User,
    @Body() pinnedLeadsDto: PinnedLeadsDto,
  ): Promise<void> {
    if (pinnedLeadsDto.action === ActionEnum.ADD) {
      return this.pinnedLeadService.createPinnedLeads(user, pinnedLeadsDto);
    }
    return this.pinnedLeadService.deletePinnedLeads(user, pinnedLeadsDto);
  }

  // @PermissionRequired(PermissionEnum.VIEW_LEAD, PermissionEnum.MANAGE_LEAD)
  @Get()
  findOwnPinnedLeads(@CurrentUser() user: User): Promise<PinnedLead[]> {
    return this.pinnedLeadService.findOwnPinnedLeads(user.id);
  }
}
