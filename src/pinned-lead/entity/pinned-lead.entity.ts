import {
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../../user/entities/user.entity';
import { Lead } from '../../lead/entities/lead.entity';
import { Exclude } from 'class-transformer';

@Entity()
@Unique(['user', 'lead'])
export class PinnedLead {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => User, (user) => user.pinnedLeads)
  user: User;

  @ManyToOne(() => Lead, (lead) => lead.pinnedLeads)
  lead: Lead;

  @Exclude()
  @CreateDateColumn()
  createdAt: string;

  @Exclude()
  @UpdateDateColumn()
  updatedAt: string;
}
