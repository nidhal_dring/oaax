import { EntityRepository, Repository } from 'typeorm';
import { PinnedLead } from '../entity/pinned-lead.entity';
import { User } from '../../user/entities/user.entity';
import { Lead } from '../../lead/entities/lead.entity';
import { ConflictException } from '@nestjs/common';
import { Errors } from '../../commun/enums/Errors.enum';

@EntityRepository(PinnedLead)
export class PinnedLeadRepository extends Repository<PinnedLead> {
  async createPinnedLead(user: User, lead: Lead): Promise<PinnedLead> {
    const pinnedLead = new PinnedLead();
    pinnedLead.user = user;
    pinnedLead.lead = lead;
    await this.save(pinnedLead);
    return this.findOne(pinnedLead.id, { relations: ['lead', 'user'] });
  }

  async createPinnedLeads(user: User, leads: Lead[]): Promise<void> {
    const pinnedLeadArray = [];
    for (const lead of leads) {
      const pinnedLead = new PinnedLead();
      pinnedLead.user = user;
      pinnedLead.lead = lead;
      pinnedLeadArray.push(pinnedLead);
    }
    await this.save(pinnedLeadArray);
  }

  async findOwnPinnedLeads(userId: string): Promise<PinnedLead[]> {
    return this.find({ where: { user: userId }, relations: ['lead', 'user'] });
  }

  async deletePinnedLead(user: User, lead: Lead): Promise<void> {
    const result = await this.delete({ lead, user });
    if (result.affected === 0) {
      throw new ConflictException(Errors.PINNED_LEAD_NOT_FOUND);
    }
  }

  async findPinnedLead(leadIds: string[]): Promise<Lead[]> {
    const query = this.createQueryBuilder('pinnedLead')
      .leftJoinAndSelect('pinnedLead.lead', 'lead')
      .leftJoinAndSelect('lead.languages', 'languages')
      .leftJoinAndSelect('lead.appointments', 'appointments')
      .leftJoinAndSelect('lead.callAgent', 'callAgent')
      .leftJoinAndSelect('lead.mainPerson', 'mainPerson')
      .leftJoinAndSelect('mainPerson.insurance', 'insurance')
      .leftJoinAndSelect('appointments.language', 'language')
      .leftJoinAndSelect('insurance.healthInsurance', 'healthInsurance')
      .leftJoinAndSelect('insurance.contractDuration', 'contractDuration')
      .leftJoinAndSelect('insurance.hospitality', 'hospitality')
      .leftJoinAndSelect('lead.files', 'files')
      .orderBy('pinnedLead.createdAt', 'ASC');
    if (leadIds.length > 0) {
      query.where('lead.id IN (:...leadIds)', {
        leadIds,
      });
    }

    const result = await query.getMany();
    return result.map((pinnedLead) => pinnedLead.lead);
  }
}
