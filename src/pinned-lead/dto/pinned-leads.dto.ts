import { IsEnum, IsString } from 'class-validator';
import { ActionEnum } from '../enum/action.enum';

export class PinnedLeadsDto {
  @IsString({ each: true })
  leads: string[];

  @IsEnum(ActionEnum)
  action: ActionEnum;
}
