import { forwardRef, Module } from '@nestjs/common';
import { PinnedLeadService } from './pinned-lead.service';
import { PinnedLeadController } from './pinned-lead.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PinnedLeadRepository } from './repository/pinned-lead.repository';
import { LeadModule } from '../lead/lead.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([PinnedLeadRepository]),
    forwardRef(() => LeadModule),
  ],
  controllers: [PinnedLeadController],
  providers: [PinnedLeadService],
  exports: [PinnedLeadService],
})
export class PinnedLeadModule {}
