import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PinnedLeadRepository } from './repository/pinned-lead.repository';
import { User } from '../user/entities/user.entity';
import { LeadService } from '../lead/lead.service';
import { PinnedLead } from './entity/pinned-lead.entity';
import { PinnedLeadsDto } from './dto/pinned-leads.dto';
import { Lead } from '../lead/entities/lead.entity';

@Injectable()
export class PinnedLeadService {
  constructor(
    @InjectRepository(PinnedLeadRepository)
    private readonly pinnedLeadRepository: PinnedLeadRepository,
    @Inject(forwardRef(() => LeadService))
    private readonly leadService: LeadService,
  ) {}

  async createPinnedLeads(
    user: User,
    pinnedLeadsDto: PinnedLeadsDto,
  ): Promise<void> {
    const leads = await this.leadService.getLeadsByIds(pinnedLeadsDto.leads);
    return this.pinnedLeadRepository.createPinnedLeads(user, leads);
  }

  async createPinnedLead(user: User, idLead: string): Promise<PinnedLead> {
    const lead = await this.leadService.findOne(idLead);
    return this.pinnedLeadRepository.createPinnedLead(user, lead);
  }

  async findOwnPinnedLeads(userId: string): Promise<PinnedLead[]> {
    return this.pinnedLeadRepository.findOwnPinnedLeads(userId);
  }

  async getLeadIdsByUserId(userId: string): Promise<string[]> {
    const leadIds = [];
    const pinnedLeads = await this.findOwnPinnedLeads(userId);
    for (const pinnedLead of pinnedLeads) {
      leadIds.push(pinnedLead.lead.id);
    }
    return leadIds;
  }

  async deletePinnedLead(user: User, idLead: string): Promise<void> {
    const lead = await this.leadService.findOne(idLead);
    return this.pinnedLeadRepository.deletePinnedLead(user, lead);
  }

  async deletePinnedLeads(
    user: User,
    pinnedLeadsDto: PinnedLeadsDto,
  ): Promise<void> {
    const leads = await this.leadService.getLeadsByIds(pinnedLeadsDto.leads);
    for (const lead of leads) {
      await this.pinnedLeadRepository.deletePinnedLead(user, lead);
    }
  }

  async findPinnedLead(leadIds: string[]): Promise<Lead[]> {
    return this.pinnedLeadRepository.findPinnedLead(leadIds);
  }
}
