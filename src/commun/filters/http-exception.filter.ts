import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { Errors } from '../enums/Errors.enum';
import { Languages } from '../enums/languages.enum';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost): unknown {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();
    const exceptionRes = exception.getResponse();

    const lang = request.get('Accept-Language');
    if (!(<any>Object).values(Languages).includes(lang)) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: status,
        timestamp: new Date().toISOString(),
        message: exceptionRes as { message: string },
        path: request.url,
      });
    }

    return response.status(status).json({
      statusCode: status,
      message:
        exceptionRes[lang] || (exceptionRes as { message: string }).message,
      timestamp: new Date().toISOString(),
      path: request.url,
    });
  }
}
