import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
} from '@nestjs/common';
import { QueryFailedError } from 'typeorm';
import { Errors } from '../enums/Errors.enum';
import { Request, Response } from 'express';
import { DatabaseQueryErrors } from '../enums/typeorm-errors.enum';
import { Languages } from '../enums/languages.enum';

@Catch(QueryFailedError)
export class QueryExceptionFilter implements ExceptionFilter {
  catch(err: any, host: ArgumentsHost): unknown {
    const response = host.switchToHttp().getResponse<Response>();
    const request = host.switchToHttp().getRequest<Request>();
    const lang = request.get('Accept-Language');
    if (!(<any>Object).values(Languages).includes(lang))
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        timestamp: new Date().toISOString(),
        path: request.url,
      });

    if (
      err.code === DatabaseQueryErrors.DUPLICATION ||
      err.code === DatabaseQueryErrors.DUPLICATION_REFERENCE
    ) {
      response.status(HttpStatus.CONFLICT).json({
        statusCode: HttpStatus.CONFLICT,
        message: Errors.CONFLICT[lang],
        timestamp: new Date().toISOString(),
        path: request.url,
      });
    } else if (
      err.code === DatabaseQueryErrors.ROW_IS_REFERENCED ||
      err.code === DatabaseQueryErrors.KEY_IS_REFERENCED
    ) {
      response.status(HttpStatus.CONFLICT).json({
        statusCode: HttpStatus.CONFLICT,
        message: Errors.REFERENCED[lang],
        timestamp: new Date().toISOString(),
        path: request.url,
      });
    } else {
      response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: Errors.INTERNAL_SERVER_ERROR[lang],
        timestamp: new Date().toISOString(),
        path: request.url,
      });
    }
  }
}
