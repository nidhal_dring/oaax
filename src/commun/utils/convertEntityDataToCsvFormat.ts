import stringify from 'csv-stringify/lib/sync';
import { Options } from 'csv-stringify';
export default function convert<T>(
  entity: T,
  data: T[],
  removedKeys?: string[],
): Buffer {
  const keys = Object.keys(entity);
  const options: Options = {
    header: true,
    cast: {
      date: (date) =>
        `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`,
    },
    delimiter: ';',
  };
  const columns = [];
  for (const key of keys) {
    if (!removedKeys.includes(key)) {
      columns.push({ key });
    }
  }

  const formatedData = stringify(data, { ...options, columns });
  return Buffer.from(formatedData);
}
