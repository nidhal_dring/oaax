// export enum Errors {
//   PERMISSION_NOT_FOUND = 'PERMISSION_NOT_FOUND',
//   USER_NOT_FOUND = 'USER_NOT_FOUND',
//   AGENT_NOT_FOUND = 'AGENT_NOT_FOUND',
//   LEAD_NOT_FOUND = 'LEAD_NOT_FOUND',
//   HOSPITALITY_NOT_FOUND = 'HOSPITALITY_NOT_FOUND',
//   CONTRACT_DURATION_NOT_FOUND = 'CONTRACT_DURATION_NOT_FOUND',
//   HEALTH_INSURANCE_NOT_FOUND = 'HEALTH_INSURANCE_NOT_FOUND',
//   APPOINTMENT_NOT_FOUND = 'APPOINTMENT_NOT_FOUND',
//   WRONG_CREDENTIALS_PROVIDED = 'WRONG_CREDENTIALS_PROVIDED',
//   ROLE_NOT_FOUND = 'ROLE_NOT_FOUND',
//   LANGUAGE_NOT_FOUND = 'LANGUAGE_NOT_FOUND',
//   COMPANY_NOT_FOUND = 'COMPANY_NOT_FOUND',
//   INTERNAL_SERVER_ERROR = 'Internal Server Error',
//   CONFLICT = 'Resource already exits !',
//   REFERENCED = 'This entity is used somewhere else or relationship field does not exists !',
//   INVALID_REGISTRATION_TOKEN = 'Registration token is either invalid or expired !',
//   WRONG_APPOINTMENT_STATUS = 'WRONG_APPOINTMENT_STATUS',
//   WRONG_APPOINTMENT_CATEGORY = 'WRONG_APPOINTMENT_CATEGORY',
//   PINNED_LEAD_NOT_FOUND = 'PINNED_LEAD_NOT_FOUND',
//   USER_PREFERENCE_NOT_FOUND = 'USER_PREFERENCE_NOT_FOUND',
// }

export class Errors {
  static readonly PERMISSION_NOT_FOUND = new Errors(
    'Permission not found',
    'Autorisation non trouvée',
  );

  static readonly USER_NOT_FOUND = new Errors(
    'User not found',
    'Utilisateur non trouvé',
  );

  static readonly AGENT_NOT_FOUND = new Errors(
    'Agent not found',
    'Agent non trouvée',
  );

  static readonly LEAD_NOT_FOUND = new Errors(
    'Lead not found',
    'Mener non trouvée',
  );

  static readonly HOSPITALITY_NOT_FOUND = new Errors(
    'Hospitality not found',
    'Hospitalité non trouvé',
  );

  static readonly CONTRACT_DURATION_NOT_FOUND = new Errors(
    'Contract Duration not found',
    'Durée du contrat introuvable',
  );

  static readonly APPOINTMENT_NOT_FOUND = new Errors(
    'Appointment not found',
    'Rendez-vous introuvable',
  );

  static readonly HEALTH_INSURANCE_NOT_FOUND = new Errors(
    'Health Insurance not found',
    'Assurance maladie introuvable',
  );

  static readonly WRONG_CREDENTIALS_PROVIDED = new Errors(
    'Wrong Credentials Provided',
    "Mauvaises informations d'identification fournies",
  );
  static readonly ROLE_NOT_FOUND = new Errors(
    'Role not found',
    'Role non trouvé',
  );

  static readonly LANGUAGE_NOT_FOUND = new Errors(
    'Language not found',
    'Langue non trouvée',
  );
  static readonly COMPANY_NOT_FOUND = new Errors(
    'Company not found',
    'Société non trouvée',
  );

  static readonly INTERNAL_SERVER_ERROR = new Errors(
    'Internal Server Error',
    'Erreur Interne du Serveur',
  );

  static readonly CONFLICT = new Errors(
    'Resource already exists',
    'La ressource existe déjà',
  );

  static readonly REFERENCED = new Errors(
    'This entity is used somewhere else or relationship field does not exist',
    "Cette entité est utilisée ailleurs ou le champ de relation n'existe pas",
  );

  static readonly INVALID_REGISTRATION_TOKEN = new Errors(
    'Registration token is either invalid or expired',
    "Le jeton d'enregistrement est invalide ou a expiré",
  );

  static readonly WRONG_APPOINTMENT_STATUS = new Errors(
    'Wrong appointment status',
    'Statut de rendez-vous erroné',
  );
  static readonly WRONG_APPOINTMENT_CATEGORY = new Errors(
    'Wrong appointment category',
    'Mauvaise catégorie de rendez-vous',
  );
  static readonly PINNED_LEAD_NOT_FOUND = new Errors(
    'Pinned not found',
    'épingler non trouvée',
  );
  static readonly USER_PREFERENCE_NOT_FOUND = new Errors(
    'User preference not found',
    'Préférence utilisateur introuvable',
  );

  static readonly INVITED_USER_NOT_FOUND = new Errors(
    'invited user not found',
    'utilisateur invité introuvable',
  );

  private constructor(
    private readonly EN: string,
    public readonly FR: string,
  ) {}
}
