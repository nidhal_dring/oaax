import {
  BadRequestException,
  createParamDecorator,
  ExecutionContext,
} from '@nestjs/common';
import { Request } from 'express';
import { Languages } from '../enums/languages.enum';

export const Lang = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request: Request = ctx.switchToHttp().getRequest();
    const lang = request.get('Accept-Language');

    if (!(<any>Object).values(Languages).includes(lang)) {
      throw new BadRequestException();
    }
    return lang;
  },
);
