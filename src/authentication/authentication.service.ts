import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { User } from '../user/entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import { Permission } from '../permission/entities/permission.entity';
import { InvitedUserService } from '../invited-users/invited-user.service';
import { UserStatus } from '../user/enum/user-status.enum';
import { LoginResponseInterface } from './interfaces/login-response.interface';
import { JwtPayload } from './jwt/jwt-payload.interface';

@Injectable()
export class AuthenticationService {
  constructor(
    private readonly usersService: UserService,
    private readonly invitedUserService: InvitedUserService,
    private jwtService: JwtService,
  ) {}

  async loginWithMicrosoft(
    microsoftUserProfile: any,
  ): Promise<LoginResponseInterface> {
    let user = await this.usersService.findUserByEmailOrThrows(
      microsoftUserProfile._json.mail,
    );
    if (user.status === UserStatus.PENDING) {
      user = await this.usersService.addUserFormMicrosoftProfile(
        user,
        microsoftUserProfile,
      );
    } else {
      user = await this.usersService.updateUserFromMicrosoftProfile(
        user.id,
        microsoftUserProfile,
      );
    }
    return await this.createResponse(user);
  }

  async createResponse(user: User): Promise<LoginResponseInterface> {
    const payload: JwtPayload = { id: user.id };
    const token = this.jwtService.sign(payload);
    return { token, user };
  }

  getPermissionsNames(user: User): string[] {
    return user.role?.permissions?.map((perm: Permission) => perm.name);
  }
}
