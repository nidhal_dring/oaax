import { Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { ApiTags } from '@nestjs/swagger';
import { MicrosoftAuthGuardGuard } from './guards/microsoft-auth.guard';
import { MsResponse } from './decorator/ms-response.decorator';
import { LoginResponseInterface } from './interfaces/login-response.interface';

@Controller('/api/authentication')
@ApiTags('Authentication')
export class AuthenticationController {
  constructor(private readonly authenticationService: AuthenticationService) {}

  @Get('login')
  @UseGuards(MicrosoftAuthGuardGuard)
  login(): string {
    return 'delegate authentication to microsoft';
  }

  @Post('redirect')
  @UseGuards(MicrosoftAuthGuardGuard)
  redirect(
    @MsResponse() msUserProfileResponse: any,
  ): Promise<LoginResponseInterface> {
    return this.authenticationService.loginWithMicrosoft(msUserProfileResponse);
  }
}
