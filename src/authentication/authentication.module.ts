import { Module } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { AuthenticationController } from './authentication.controller';
import { UserModule } from '../user/user.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { configService } from '../config/config.service';
import { JwtStrategy } from './jwt/jwt.strategy';
import { MicrosoftOauth2Strategy } from './strategies/microsoft-oauth2.startegy';
import { InvitedUserModule } from '../invited-users/invited-user.module';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: configService.getJWTConfig().JWT_SECRET,
      signOptions: {
        expiresIn: configService.getJWTConfig().JWT_EXPIRATION,
      },
    }),
    UserModule,
    InvitedUserModule,
  ],
  controllers: [AuthenticationController],
  providers: [AuthenticationService, MicrosoftOauth2Strategy, JwtStrategy],
  exports: [JwtModule, JwtStrategy],
})
export class AuthenticationModule {}
