import { User } from '../../user/entities/user.entity';

export interface LoginResponseInterface {
  token: string;
  user: User;
}
