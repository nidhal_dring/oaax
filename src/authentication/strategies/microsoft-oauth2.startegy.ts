import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { configService } from '../../config/config.service';

import { Strategy } from 'passport-microsoft';

@Injectable()
export class MicrosoftOauth2Strategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      clientID: configService.getMicrosoftAuthConfig().CLIENT_ID,
      clientSecret: configService.getMicrosoftAuthConfig().CLIENT_SECRET,
      callbackURL: configService.getMicrosoftAuthConfig().CALLBACK_URL,
      scope: [configService.getMicrosoftAuthConfig().SCOPE],
    });
  }

  async validate(accessToken, refreshToken, profile, done) {
    if (!accessToken) {
      throw new UnauthorizedException();
    }
    return {
      accessToken,
      refreshToken,
      profile,
      done,
    };
  }
}
