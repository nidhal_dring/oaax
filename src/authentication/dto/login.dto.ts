import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class LoginDto {
  @ApiProperty({ default: 'admin@admin.com' })
  @IsEmail()
  email: string;

  @ApiProperty({ default: 'admin' })
  @IsString()
  @IsNotEmpty()
  password: string;
}
