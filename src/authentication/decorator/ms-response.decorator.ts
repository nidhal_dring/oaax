import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const MsResponse = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return request.user.profile;
  },
);
