import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { RoleService } from './role.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { ApiBearerAuth, ApiHeader, ApiTags } from '@nestjs/swagger';
import { ResponseRoleDto } from './dto/response-role.dto';
import { AuthGuard } from '@nestjs/passport';
import { PermissionEnum } from '../permission/enum/permission.enum';
import { PermissionRequired } from '../permission/decorator/permission.decorator';
import { Languages } from 'src/commun/enums/languages.enum';

@Controller('api/role')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@ApiTags('Role')
@ApiHeader({ name: 'Accept-Language', enum: Languages })
export class RoleController {
  constructor(private readonly roleService: RoleService) {}

  @Post()
  create(@Body() createRoleDto: CreateRoleDto): Promise<ResponseRoleDto> {
    return this.roleService.createRole(createRoleDto);
  }

  @Get()
  findAll(): Promise<ResponseRoleDto[]> {
    return this.roleService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<ResponseRoleDto> {
    return this.roleService.findOne(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateRoleDto: UpdateRoleDto,
  ): Promise<ResponseRoleDto> {
    return this.roleService.update(id, updateRoleDto);
  }

  // @PermissionRequired(PermissionEnum.DELETE_ROLE, PermissionEnum.MANAGE_ROLE)
  @Delete(':id')
  remove(@Param('id') id: string): Promise<void> {
    return this.roleService.remove(id);
  }
}
