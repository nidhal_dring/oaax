import { EntityRepository, Repository } from 'typeorm';
import { Role } from '../entities/role.entity';
import { CreateRoleDto } from '../dto/create-role.dto';
import { UpdateRoleDto } from '../dto/update-role.dto';
import { NotFoundException } from '@nestjs/common';
import { Errors } from '../../commun/enums/Errors.enum';

@EntityRepository(Role)
export class RoleRepository extends Repository<Role> {
  async createRole(createRoleDto: CreateRoleDto): Promise<Role> {
    const role = new Role();
    Object.assign(role, createRoleDto);
    await this.save(role);
    return this.findOne(role.id, { relations: ['permissions'] });
  }

  async findAllRoles(): Promise<Role[]> {
    return this.find({ relations: ['permissions'] });
  }

  async findOneByName(name: string): Promise<Role> {
    const role = await this.findOne({
      where: { name },
      relations: ['permissions'],
    });
    if (!role) {
      throw new NotFoundException(Errors.ROLE_NOT_FOUND);
    }
    return role;
  }

  async findRole(id: string): Promise<Role> {
    const role = await this.findOne(id, { relations: ['permissions'] });
    if (!role) {
      throw new NotFoundException(Errors.ROLE_NOT_FOUND);
    }
    return role;
  }

  async updateRole(id: string, updateRoleDto: UpdateRoleDto): Promise<Role> {
    const role = await this.findRole(id);
    Object.assign(role, updateRoleDto);
    await this.save(role);
    return this.findRole(role.id);
  }

  async deleteRole(id: string) {
    const result = await this.delete(id);
    if (result.affected === 0) {
      throw new NotFoundException(Errors.ROLE_NOT_FOUND);
    }
  }
}
