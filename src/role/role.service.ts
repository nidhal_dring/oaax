import { Injectable } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { RoleRepository } from './repository/role.repository';
import { Role } from './entities/role.entity';

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(RoleRepository)
    private readonly roleRepository: RoleRepository,
  ) {}

  createRole(createRoleDto: CreateRoleDto): Promise<Role> {
    return this.roleRepository.createRole(createRoleDto);
  }

  findAll(): Promise<Role[]> {
    return this.roleRepository.findAllRoles();
  }

  findOne(id: string): Promise<Role> {
    return this.roleRepository.findRole(id);
  }

  findOneByName(name: string): Promise<Role> {
    return this.roleRepository.findOneByName(name);
  }

  update(id: string, updateRoleDto: UpdateRoleDto): Promise<Role> {
    return this.roleRepository.updateRole(id, updateRoleDto);
  }

  remove(id: string): Promise<void> {
    return this.roleRepository.deleteRole(id);
  }
}
