import { Connection, Repository } from 'typeorm';
import { Role } from '../entities/role.entity';
import roles from './roles.json';

export class RoleSeed {
  private readonly connection: Connection;
  private readonly roleRepository: Repository<Role>;

  constructor(connection: Connection) {
    this.connection = connection;
    this.roleRepository = connection.getRepository(Role);
  }

  async run(): Promise<void> {
    await this.deleteAllRoles();
    await this.createRoles();
  }

  async deleteAllRoles(): Promise<void> {
    await this.roleRepository.delete({});
  }

  async createRoles(): Promise<void> {
    await Promise.all(
      roles.map((role) => {
        this.roleRepository.save(role as Role);
      }),
    );
  }
}
