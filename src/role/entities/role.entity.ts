import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { User } from '../../user/entities/user.entity';
import { Permission } from '../../permission/entities/permission.entity';
import { InvitedUser } from '../../invited-users/entities/invited-user.entity';

@Entity()
export class Role {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  name: string;

  @OneToMany(() => User, (user) => user.role, { nullable: false })
  users: User[];

  @OneToMany(() => InvitedUser, (invitedUser) => invitedUser.role)
  invitedUsers: InvitedUser[];

  @ManyToMany(() => Permission, { cascade: true, eager: true })
  @JoinTable()
  permissions!: Permission[];

  @Exclude()
  @CreateDateColumn()
  createdAt: string;

  @Exclude()
  @UpdateDateColumn()
  updatedAt: string;
}
