class permissionDto {
  id: string;
  name: string;
}

export class ResponseRoleDto {
  id: string;
  name: string;
  permissions: permissionDto[];
}
