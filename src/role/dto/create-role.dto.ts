import {
  IsArray,
  IsNotEmpty,
  IsString,
  IsUUID,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

class PermissionDto {
  @ApiProperty({
    required: true,
    default: 'a97bab29-1971-4a7f-83a8-06b20a4bd733',
  })
  @IsUUID()
  id: string;
}

export class CreateRoleDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @ValidateNested({ each: true })
  @IsArray()
  @Type(() => PermissionDto)
  permissions: PermissionDto[];
}
