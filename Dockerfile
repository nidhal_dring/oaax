FROM node:14-slim

RUN npm install -g @nestjs/cli

WORKDIR /app

COPY . .

RUN npm i 

RUN mkdir -p ./logs ./uploads

CMD ["npm", "run", "start:dev"]
